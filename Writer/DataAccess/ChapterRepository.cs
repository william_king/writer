﻿using Finisar.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.Entities;

namespace Writer.DataAccess.DataAccess
{
	public class ChapterRepository
	{
		private string _projectDbName;

		public int AddChapter(string path, string projectName, Chapter chapter)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertChapter, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = chapter.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = chapter.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = chapter.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = chapter.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SortOrder",
					Value = chapter.SortOrder
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				chapter.Id = (int)LastRowID64;
			}

			return chapter.Id;
		}

		public Chapter GetChapter(string path, string projectName, string title)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetChapter, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@title",
					Value = title
				};
				command.Parameters.Add(param);

				Chapter chapter = null;

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						chapter = new Chapter()
						{
							Id = int.Parse(reader["Id"].ToString()),
							ProjectId = int.Parse(reader["ProjectId"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							SortOrder = int.Parse(reader["SortOrder"].ToString())
						};
					}
				}
				return chapter;
			}
		}

		public List<Chapter> GetChapters(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Chapter> chapters = new List<Chapter>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetChapters, con);
				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Chapter chapter = new Chapter()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString()),
							SortOrder = int.Parse(reader["SortOrder"].ToString())
						};

						chapters.Add(chapter);
					}
				}
			}
			return chapters;
		}

		public List<Scene> GetScenes(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Scene> scenes = new List<Scene>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetScenesByChapter, con); 
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@chapterid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Scene chapter = new Scene()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ChapterId = int.Parse(reader["ChapterId"].ToString()),
							PlotId = int.Parse(reader["PlotId"].ToString()),
							LocationId = int.Parse(reader["LocationId"].ToString()),
							ItemSectionId = int.Parse(reader["ItemSectionId"].ToString()),
							ItemStatusId = int.Parse(reader["ItemStatusId"].ToString()),
							ItemLabelId = int.Parse(reader["ItemLabelId"].ToString()),
							PointOfView = int.Parse(reader["PointOfView"].ToString()),
							SceneContent = reader["SceneContent"].ToString(),
							PlannedWordCount = int.Parse(reader["PlannedWordCount"].ToString()),
							StartDate = reader["StartDate"].ToString(),
							StartTime = reader["StartTime"].ToString(),
							EndDate = reader["EndDate"].ToString(),
							EndTime = reader["EndTime"].ToString(),
							SortOrder = int.Parse(reader["SortOrder"].ToString())
						};

						scenes.Add(chapter);
					}
				}
			}
			return scenes;
		}
	}
}
