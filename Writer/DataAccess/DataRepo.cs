﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.DataAccess
{
	public class DataRepo
	{
		public string ProjectDbName { get; set; }
		public string ProjectName { get; set; }
		public void CreateNewRepo(string name)
		{
			ProjectDbName = name + ".sqlite";
			if (!File.Exists(ProjectDbName))
			{
				SQLiteConnection.CreateFile(ProjectDbName);

				string sql = @"CREATE TABLE tblProject(
								ID INTEGER PRIMARY KEY AUTOINCREMENT ,
								ProjectName			TEXT		NOT NULL,
								ProjectDescription	TEXT,
								Author				TEXT,
								AuthorBio			TEXT,
								OutlineStart		REAL,
								OutlineEnd			REAL,
								DraftStart			REAL,
								DraftEnd			REAL,
								FirstEditStart		REAL,
								FirstEditEnd		REAL,
								SecondEditStart		REAL,
								SecondEditEnd		REAL,
								FinalStart			REAL,
								FinalEnd			REAL
							);";
				SQLiteConnection con = new SQLiteConnection("Data Source=" + ProjectDbName + ";Version=3;");
				con.Open();
				SQLiteCommand cmd = new SQLiteCommand(sql, con);
				cmd.ExecuteNonQuery();
				con.Close();

			}
			AddRecord();
		}

		public void AddRecord()
		{
			SQLiteCommand cmd = new SQLiteCommand();
			SQLiteConnection con = new SQLiteConnection("Data Source=" + ProjectDbName + ";Version=3;");
			con.Open();
			cmd.Connection = con;
			cmd.CommandText = "insert into tblProject(ProjectName) values ('TestProject')";
			cmd.ExecuteNonQuery();
			con.Close();
			GetProject();
		}

		public void GetProject()
		{
			SQLiteConnection con = new SQLiteConnection("Data Source=" + ProjectDbName + ";Version=3;");
			SQLiteDataAdapter da = new SQLiteDataAdapter("Select *From tblProject", con);
			DataSet ds = new DataSet();
			con.Open();
			da.Fill(ds, "tblProject");
			ProjectName = ds.Tables["tblProject"].Columns["ProjectName"].ToString();
			con.Close();
		}

		private void button2_Click(object sender, EventArgs e) //update
		{
			SQLiteCommand cmd = new SQLiteCommand();
			SQLiteConnection con = new SQLiteConnection("Data Source=" + ProjectDbName + ";Version=3;");
			cmd.Connection = con;
			//cmd.CommandText = "update Student set FirstName='" + textBox2.Text + "',LastName='" + textBox3.Text + "' where ID=" + textBox1.Text + "";
			cmd.ExecuteNonQuery();
			con.Close();
		}

		private void button3_Click(object sender, EventArgs e) //delete
		{
			SQLiteCommand cmd = new SQLiteCommand();
			SQLiteConnection con = new SQLiteConnection("Data Source=" + ProjectDbName + ";Version=3;");
			cmd.Connection = con;
			//cmd.CommandText = "delete from Student where ID=" + textBox1.Text + "";
			cmd.ExecuteNonQuery();
			con.Close();
		}
	}
}
