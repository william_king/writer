﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.DataAccess.DataAccess
{
	public static class ProjectSprocs
	{
		public static readonly string GetProjectLayout = "Select * From tblProjectLayout where Title = @title";
		public static readonly string DeleteProjectLayout = "Delete From tblProjectLayout where Title = @title";


		//get records
		public static readonly string GetChapter = "Select * From tblChapter where Title = @title";
		public static readonly string GetChapters = "Select * From tblChapter order by SortOrder";
		public static readonly string GetCustomFields = "Select * From tblCustomField";
		public static readonly string GetLinkedMetaDataItems = "Select * From tblLinkedMetaDataItem Where ObjectType = @objecttype And ObjectId = @objectid";
		public static readonly string GetLinkedMetaDataItem = "Select * From tblLinkedMetaDataItem Where Id = @id";
		public static readonly string GetMetaDataItems = "Select * From tblMetaDataItem";
		public static readonly string GetNotesByObjectType = "Select * From tblNote Where ObjectType = @objecttype And ObjectId = @objectid";
		public static readonly string GetRevisionsByObjectType = "Select * From tblRevision Where ObjectType = @objecttype And ObjectId = @objectid";
		public static readonly string GetWritingSessions = "Select * From tblWritingSession Order By StartDate";
		public static readonly string GetScenesByChapter = "Select * From tblScene Where ChapterId = @chapterid order by SortOrder";
		public static readonly string GetPlot = "Select * From tblPlot Where Id = @id";
		public static readonly string GetPlots = "Select * From tblPlot";
		public static readonly string GetLocation = "Select * From tblLocation Where Id = @id";
		public static readonly string GetLocations = "Select * From tblLocation Where ProjectId = @projectid";
		public static readonly string GetTheme = "Select * From tblTheme Where Id = @id";
		public static readonly string GetThemes = "Select * From tblTheme";
		public static readonly string GetCharacter = "Select * From tblCharacter Where Id = @id";
		public static readonly string GetCharacters = "Select * From tblCharacter";
		public static readonly string GetStoryEvent = "Select * From tblStoryEvent Where Id = @id";
		public static readonly string GetStoryObject = "Select * From tblStoryObject Where Id = @id";
		public static readonly string GetStoryEvents = "Select * From tblStoryEvent";
		public static readonly string GetStoryObjects = "Select * From tblStoryObject";
		public static readonly string GetItemSection = "Select * From tblItemSection Where Id = @id";
		public static readonly string GetItemSectionsByGroup = "Select * From tblItemSection Where ItemSectionGroupId = @groupid";
		public static readonly string GetItemSections = "Select * From tblItemSection";
		public static readonly string GetItemLabel = "Select * From tblItemLabel Where Id = @id";
		public static readonly string GetItemLabels = "Select * From tblItemLabel";
		public static readonly string GetItemStatus = "Select * From tblItemStatus Where Id = @id";
		public static readonly string GetItemStatuses = "Select * From tblItemStatus";
		public static readonly string GetItemSectionGroups = "Select * From tblItemSectionGroup";
		public static readonly string GetGroupSection = "Select * From tblGroupSection Where Id = @id";
		public static readonly string GetGroupItem = "Select * From tblGroupItem Where Id = @id";
		public static readonly string GetLinkedGroupSection = "Select * From tblLinkedGroupSection Where Id = @id";
		public static readonly string GetLinkedGroupSections = "Select * From tblLinkedGroupSection Where ObjectType = @objecttype And ObjectId = @objectid";
		public static readonly string GetLinkedGroupItems = "Select * From tblLinkedGroupItem Where ObjectType = @objecttype And ObjectId = @objectid";
		public static readonly string GetLinkedGroupItem = "Select * From tblLinkedGroupItem Where Id = @id";
		public static readonly string GetThemesByScene = "Select * From tblSceneTheme Where SceneId = @sceneid";
		public static readonly string GetThemeForScene = "Select * From tblSceneTheme Where SceneId = @sceneid and ThemeId = @themeid";
		public static readonly string GetCharactersByScene = "Select * From tblSceneCharacter Where SceneId = @sceneid";
		public static readonly string GetCharacterForScene = "Select * From tblSceneCharacter Where SceneId = @sceneid and ThemeId = @characterid";
		public static readonly string GetObjectsByScene = "Select * From tblSceneObject Where SceneId = @sceneid";
		public static readonly string GetObjectForScene = "Select * From tblSceneObject Where SceneId = @sceneid and StoryObjectId = @objectid";
		public static readonly string GetEventsByScene = "Select * From tblSceneEvent Where SceneId = @sceneid";
		public static readonly string GetEventForScene = "Select * From tblSceneEvent Where SceneId = @sceneid and StoryEventId = @eventid";

		//add records
		public static readonly string InsertChapter = "Insert Into tblChapter(ProjectId,Title,Synopsis,DateCreated,SortOrder) Values(@ProjectId,@Title,@Synopsis,@DateCreated,@SortOrder)";
		public static readonly string InsertCharacter = "Insert Into tblCharacter(Title,Synopsis,DateCreated,Name,Surname,Image) Values(@Title,@Synopsis,@DateCreated,@Name,@Surname,@Image)";
		public static readonly string InsertCustomField = "Insert Into tblCustomField(ProjectId,Title,Synopsis,DateCreated) Values (@ProjectId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertGroupSection = "Insert Into tblGroupSection(Title,Synopsis,DateCreated) Values (@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertLinkedGroupSection = "Insert Into tblLinkedGroupSection(ObjectType,ObjectId) Values (@ObjectType,@ObjectId)";
		public static readonly string InsertGroupItem = "Insert Into tblGroupItem(GroupSectionId,Title,Synopsis,DateCreated) Values (@GroupSectionId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertLinkedGroupItem = "Insert Into tblLinkedGroupItem(LinkedGroupSectionId,GroupItemId,ObjectType,ObjectId) Values (@LinkedGroupSectionId,@GroupItemId,@ObjectType,@ObjectId)";
		public static readonly string InsertItemLabel = "Insert Into tblItemLabel(Title,Synopsis,DateCreated,LabelColor) Values (@Title,@Synopsis,@DateCreated,@LabelColor)";
		public static readonly string InsertItemSection = "Insert Into tblItemSection(ItemSectionGroupId,Title,Synopsis,DateCreated,SectionColor) Values (@ItemSectionGroupId,@Title,@Synopsis,@DateCreated,@SectionColor)";
		public static readonly string InsertItemSectionGroup = "Insert Into tblItemSectionGroup(Title,Synopsis,DateCreated) Values (@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertItemStatus = "Insert Into tblItemStatus(ProjectId,Title,Synopsis,DateCreated,StatusColor) Values (@ProjectId,@Title,@Synopsis,@DateCreated,@StatusColor)";
		public static readonly string InsertLocation = "Insert Into tblLocation(ProjectId,Title,Synopsis,DateCreated,Image) Values (@ProjectId,@Title,@Synopsis,@DateCreated,@Image)";
		public static readonly string InsertMetaDataItem = "Insert Into tblMetaDataItem(Title,Synopsis,DateCreated,MetaDataType,CustomFormat,ForeColor,BackColor) Values (@Title,@Synopsis,@DateCreated,@MetaDataType,@CustomFormat,@ForeColor,@BackColor)";
		public static readonly string InsertLinkedMetaDataItem = "Insert Into tblLinkedMetaDataItem(MetaDataItemId,ObjectType,ObjectId,ForeColor,BackColor) Values (@MetaDataItemId,@ObjectType,@ObjectId,@ForeColor,@BackColor)";
		public static readonly string InsertNote = "Insert Into tblNote(ObjectType,ObjectId,Title,Synopsis,DateCreated,ReminderDate) Values (@ObjectType,@ObjectId,@Title,@Synopsis,@DateCreated,@ReminderDate)";
		public static readonly string InsertPlot = "Insert Into tblPlot(ProjectId,Title,Synopsis,DateCreated) Values (@ProjectId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertProjectLayout = "Insert Into tblProjectLayout(Version,Title,Author,Premise,DateCreated,PlannedCompleteDate,CompleteDate,Synopsis,LastFullBackup,WordTarget,IsCompleted,WordTargetStartDate,WordTargetEndDate,WordCountStart,LastViewedSceneId,DesktopPath) Values (@Version,@Title,@Author,@Premise,@DateCreated,@PlannedCompleteDate,@CompleteDate,@Synopsis,@LastFullBackup,@WordTarget,@IsCompleted,@WordTargetStartDate,@WordTargetEndDate,@WordCountStart,@LastViewedSceneId,@DesktopPath);";
		public static readonly string InsertResearchItem = "Insert Into tblResearchItem(ProjectId,DateCreated,Url) Values (@ProjectId,@DateCreated,@Url)";
		public static readonly string InsertRevision = "Insert Into tblRevision(ObjectType,ObjectId,Title,Synopsis,DateCreated) Values (@ObjectType,@ObjectId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertScene = "Insert Into tblScene(ChapterId,PlotId,LocationId,ItemSectionId,ItemStatusId,ItemLabelId,PointOfView,Title,Synopsis,DateCreated,SceneContent,PlannedWordCount,StartDate,StartTime,EndDate,EndTime,SortOrder) Values (@ChapterId,@PlotId,@LocationId,@ItemSectionId,@ItemStatusId,@ItemLabelId,@PointOfView,@Title,@Synopsis,@DateCreated,@SceneContent,@PlannedWordCount,@StartDate,@StartTime,@EndDate,@EndTime,@SortOrder)";
		public static readonly string InsertSceneCharacter = "Insert Into tblSceneCharacter(SceneId,CharacterId) Values (@SceneId,@CharacterId)";
		public static readonly string InsertSceneEvent = "Insert Into tblSceneEvent(SceneId,StoryEventId) Values (@SceneId,@StoryEventId)";
		public static readonly string InsertSceneObject = "Insert Into tblSceneObject(SceneId,StoryObjectId) Values (@SceneId,@StoryObjectId)";
		public static readonly string InsertSceneTheme = "Insert Into tblSceneTheme(SceneId,ThemeId) Values (@SceneId,@ThemeId)";
		public static readonly string InsertStoryObject = "Insert Into tblStoryObject(ProjectId,Title,Synopsis,DateCreated,Image) Values (@ProjectId,@Title,@Synopsis,@DateCreated,@Image)";
		public static readonly string InsertStoryEvent = "Insert Into tblStoryEvent(ProjectId,Title,Synopsis,DateCreated) Values (@ProjectId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertTarget = "Insert Into tblTarget(ProjectId,DateCreated,OutlineStart,OutlineEnd, DraftStart,DraftEnd,FirstEditStart,FirstEditEnd,SecondEditStart,SecondEditEnd,FinalEditStart,FinalEditEnd) Values (@ProjectId,@DateCreated,@OutlineStart,@OutlineEnd,@DraftStart,@DraftEnd,@FirstEditStart,@FirstEditEnd,@SecondEditStart,@SecondEditEnd,@FinalEditStart,@FinalEditEnd)";
		public static readonly string InsertTheme = "Insert Into tblTheme(ProjectId,Title,Synopsis,DateCreated) Values (@ProjectId,@Title,@Synopsis,@DateCreated)";
		public static readonly string InsertWritingSession = "Insert Into tblWritingSession(ProjectId,DateCreated,StartDate,EndDate,WordCountTarget,WordCount) Value (@ProjectId,@DateCreated,@StartDate,@EndDate,@WordCountTarget,@WordCount)";


	}
}
