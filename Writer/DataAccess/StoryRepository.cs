﻿using Finisar.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.Entities;

namespace Writer.DataAccess.DataAccess
{
	public class StoryRepository
	{
		private string _projectDbName;

		public List<Location> GetLocations(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Location> locations = new List<Location>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLocations, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@projectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Location location = new Location()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString()),
							Image = reader["Image"].ToString()
						};

						locations.Add(location);
					}
				}
			}
			return locations;
		}

		public Plot GetPlot(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			Plot plot = null;

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetPlot, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						plot = new Plot();

						plot.Id = int.Parse(reader["Id"].ToString());
						plot.Title = reader["Title"].ToString();
						plot.Synopsis = reader["Synopsis"].ToString();
						plot.DateCreated = reader["DateCreated"].ToString();
						plot.ProjectId = int.Parse(reader["ProjectId"].ToString());
					}
				}
			}
			return plot;
		}

		public List<Plot> GetPlots(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Plot> plots = new List<Plot>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetPlots, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Plot plot = new Plot()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString())
						};

						plots.Add(plot);
					}
				}
			}
			return plots;
		}

		public int AddLocation(string path, string projectName, Location location)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ProjectId,Title,Synopsis,DateCreated,Image

				var command = new SQLiteCommand(ProjectSprocs.InsertLocation, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = location.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = location.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = location.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = location.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Image",
					Value = location.Image
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				location.Id = (int)LastRowID64;
			}

			return location.Id;
		}

		public Location GetLocation(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			Location location = new Location();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLocation, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						location.Id = int.Parse(reader["Id"].ToString());
						location.Title = reader["Title"].ToString();
						location.Synopsis = reader["Synopsis"].ToString();
						location.DateCreated = reader["DateCreated"].ToString();
						location.ProjectId = int.Parse(reader["ProjectId"].ToString());
						location.Image = reader["Image"].ToString();
					}
				}
			}
			return location;
		}

		public int AddCharacter(string path, string projectName, Character character)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//@Title,@Synopsis,@DateCreated,@Name,@Surname,@Image

				var command = new SQLiteCommand(ProjectSprocs.InsertCharacter, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = character.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = character.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = character.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Name",
					Value = character.Name
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Surname",
					Value = character.Surname
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Image",
					Value = character.Image
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				character.Id = (int)LastRowID64;
			}

			return character.Id;
		}

		public int AddScene(string path, string projectName, Scene scene)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ChapterId,PlotId,LocationId,ItemSectionId,ItemStatusId,ItemLabelId,PointOfView,Title,
				//Synopsis,DateCreated,SceneContent,PlannedWordCount,StartDate,StartTime,EndDate,EndTime,SortOrder

				var command = new SQLiteCommand(ProjectSprocs.InsertScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ChapterId",
					Value = scene.ChapterId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@PlotId",
					Value = scene.PlotId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@LocationId",
					Value = scene.LocationId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ItemSectionId",
					Value = scene.ItemSectionId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ItemStatusId",
					Value = scene.ItemStatusId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ItemLabelId",
					Value = scene.ItemLabelId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@PointOfView",
					Value = scene.PointOfView
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = scene.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = scene.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = scene.DateCreated
				 };
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SceneContent",
					Value = scene.SceneContent
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@PlannedWordCount",
					Value = scene.PlannedWordCount
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@StartDate",
					Value = scene.StartDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@StartTime",
					Value = scene.StartTime
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@EndDate",
					Value = scene.EndDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@EndTime",
					Value = scene.EndTime
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SortOrder",
					Value = scene.SortOrder
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				scene.Id = (int)LastRowID64;
			}

			return scene.Id;
		}

		public List<Character> GetCharacters(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Character> characters = new List<Character>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetCharacters, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Character character = new Character()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							Name = reader["Name"].ToString(),
							Surname = reader["Surname"].ToString()
						};

						characters.Add(character);
					}
				}
			}

			return characters;
		}

		public List<SceneCharacter> GetSceneCharacters(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<SceneCharacter> characters = new List<SceneCharacter>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetCharactersByScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SceneCharacter character = new SceneCharacter()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							SceneId = int.Parse(reader["SceneId"].ToString()),
							CharacterId = int.Parse(reader["CharacterId"].ToString())
					};

						characters.Add(character);
					}
				}
			}
			return characters;
		}

		public SceneCharacter GetCharacterForScene(string path, string projectName, int sceneId, int characterId)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			SceneCharacter character = new SceneCharacter();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetCharacterForScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = sceneId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@characterid",
					Value = characterId
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						character.Id = int.Parse(reader["Id"].ToString());
						character.Title = reader["Title"].ToString();
						character.Synopsis = reader["Synopsis"].ToString();
						character.DateCreated = reader["DateCreated"].ToString();
						character.SceneId = int.Parse(reader["SceneId"].ToString());
						character.CharacterId = int.Parse(reader["CharacterId"].ToString());
					}
				}
			}
			return character;
		}

		public Character GetCharacter(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			Character character = new Character();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetCharacter, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						character.Id = int.Parse(reader["Id"].ToString());
						character.Title = reader["Title"].ToString();
						character.Synopsis = reader["Synopsis"].ToString();
						character.DateCreated = reader["DateCreated"].ToString();
						character.Name = reader["Name"].ToString();
						character.Surname = reader["Surname"].ToString();
						character.Image = reader["Image"].ToString();
					}
				}
			}
			return character;
		}

		public int AddTheme(string path, string projectName, Theme theme)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ProjectId,Title,Synopsis,DateCreated

				var command = new SQLiteCommand(ProjectSprocs.InsertTheme, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = theme.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = theme.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = theme.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = theme.ProjectId
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				theme.Id = (int)LastRowID64;
			}

			return theme.Id;
		}

		public List<SceneTheme> GetSceneThemes(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<SceneTheme> themes = new List<SceneTheme>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetThemesByScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SceneTheme theme = new SceneTheme()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							SceneId = int.Parse(reader["SceneId"].ToString()),
							ThemeId = int.Parse(reader["ThemeId"].ToString())
					};

						themes.Add(theme);
					}
				}
			}
			return themes;
		}

		public SceneTheme GetThemeForScene(string path, string projectName, int sceneId, int themeId)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			SceneTheme theme = new SceneTheme();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetThemeForScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = sceneId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@themeid",
					Value = themeId
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						theme.Id = int.Parse(reader["Id"].ToString());
						theme.Title = reader["Title"].ToString();
						theme.Synopsis = reader["Synopsis"].ToString();
						theme.DateCreated = reader["DateCreated"].ToString();
						theme.SceneId = int.Parse(reader["SceneId"].ToString());
						theme.ThemeId = int.Parse(reader["ThemeId"].ToString());
					}
				}
			}
			return theme;
		}

		public Theme GetTheme(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			Theme theme = new Theme();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetTheme, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						theme.Id = int.Parse(reader["Id"].ToString());
						theme.Title = reader["Title"].ToString();
						theme.Synopsis = reader["Synopsis"].ToString();
						theme.DateCreated = reader["DateCreated"].ToString();
						theme.ProjectId = int.Parse(reader["ProjectId"].ToString());
					}
				}
			}
			return theme;
		}

		public List<Theme> GetThemes(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Theme> themes = new List<Theme>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetThemes, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Theme theme = new Theme()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString())
						};

						themes.Add(theme);
					}
				}
			}
			return themes;
		}

		public int AddStoryObject(string path, string projectName, StoryObject storyObject)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ProjectId,Title,Synopsis,DateCreated,Image

				var command = new SQLiteCommand(ProjectSprocs.InsertStoryObject, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = storyObject.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = storyObject.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = storyObject.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = storyObject.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Image",
					Value = storyObject.Image
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				storyObject.Id = (int)LastRowID64;
			}

			return storyObject.Id;
		}

		public List<StoryObject> GetStoryObjects(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<StoryObject> storyObjects = new List<StoryObject>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetStoryObjects, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						StoryObject storyObject = new StoryObject()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString())
						};

						storyObjects.Add(storyObject);
					}
				}
			}

			return storyObjects;
		}

		public List<StoryEvent> GetStoryEvents(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<StoryEvent> storyEvents = new List<StoryEvent>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetStoryEvents, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						StoryEvent storyEvent = new StoryEvent()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString())
						};

						storyEvents.Add(storyEvent);
					}
				}
			}

			return storyEvents;
		}

		public List<SceneObject> GetSceneObjects(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<SceneObject> objects = new List<SceneObject>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetObjectsByScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SceneObject sceneObject = new SceneObject()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							SceneId = int.Parse(reader["SceneId"].ToString()),
							StoryObjectId = int.Parse(reader["StoryObjectId"].ToString())
						};

						objects.Add(sceneObject);
					}
				}
			}
			return objects;
		}

		public SceneObject GetObjectForScene(string path, string projectName, int sceneId, int objectId)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			SceneObject sceneObject = new SceneObject();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetObjectForScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = sceneId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = objectId
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						sceneObject.Id = int.Parse(reader["Id"].ToString());
						sceneObject.Title = reader["Title"].ToString();
						sceneObject.Synopsis = reader["Synopsis"].ToString();
						sceneObject.DateCreated = reader["DateCreated"].ToString();
						sceneObject.SceneId = int.Parse(reader["SceneId"].ToString());
						sceneObject.StoryObjectId = int.Parse(reader["StoryObjectId"].ToString());
					}
				}
			}
			return sceneObject;
		}

		public StoryObject GetStoryObject(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			StoryObject storyObject = new StoryObject();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetStoryObject, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						storyObject.Id = int.Parse(reader["Id"].ToString());
						storyObject.Title = reader["Title"].ToString();
						storyObject.Synopsis = reader["Synopsis"].ToString();
						storyObject.DateCreated = reader["DateCreated"].ToString();
						storyObject.ProjectId = int.Parse(reader["ProjectId"].ToString());
					}
				}
			}
			return storyObject;
		}

		public int AddStoryEvent(string path, string projectName, StoryEvent storyEvent)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ProjectId,Title,Synopsis,DateCreated

				var command = new SQLiteCommand(ProjectSprocs.InsertStoryEvent, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = storyEvent.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = storyEvent.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = storyEvent.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = storyEvent.ProjectId
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				storyEvent.Id = (int)LastRowID64;
			}

			return storyEvent.Id;
		}



		public List<SceneEvent> GetSceneEvents(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<SceneEvent> sceneEvents = new List<SceneEvent>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetEventsByScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SceneEvent sceneEvent = new SceneEvent()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							SceneId = int.Parse(reader["SceneId"].ToString()),
							StoryEventId = int.Parse(reader["StoryEventId"].ToString())
						};

						sceneEvents.Add(sceneEvent);
					}
				}
			}
			return sceneEvents;
		}

		public SceneEvent GetEventForScene(string path, string projectName, int sceneId, int objectId)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			SceneEvent sceneEvent = new SceneEvent();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetEventForScene, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@sceneid",
					Value = sceneId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = objectId
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						sceneEvent.Id = int.Parse(reader["Id"].ToString());
						sceneEvent.Title = reader["Title"].ToString();
						sceneEvent.Synopsis = reader["Synopsis"].ToString();
						sceneEvent.DateCreated = reader["DateCreated"].ToString();
						sceneEvent.SceneId = int.Parse(reader["SceneId"].ToString());
						sceneEvent.StoryEventId = int.Parse(reader["StoryEventId"].ToString());
					}
				}
			}
			return sceneEvent;
		}

		public StoryEvent GetStoryEvent(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			StoryEvent storyEvent = new StoryEvent();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetStoryEvent, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						storyEvent.Id = int.Parse(reader["Id"].ToString());
						storyEvent.Title = reader["Title"].ToString();
						storyEvent.Synopsis = reader["Synopsis"].ToString();
						storyEvent.DateCreated = reader["DateCreated"].ToString();
						storyEvent.ProjectId = int.Parse(reader["ProjectId"].ToString());
					}
				}
			}
			return storyEvent;
		}

		public int AddPlot(string path, string projectName, Plot plot)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				//ProjectId,Title,Synopsis,DateCreated

				var command = new SQLiteCommand(ProjectSprocs.InsertPlot, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = plot.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = plot.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = plot.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = plot.ProjectId
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

					plot.Id = (int)LastRowID64;
			}

			return plot.Id;
		}

		

	}

}
