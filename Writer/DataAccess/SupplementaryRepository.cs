﻿using Finisar.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.Entities;

namespace Writer.DataAccess.DataAccess
{
	public class SupplementaryRepository
	{
		private string _projectDbName;

		public int AddItemSectionGroup(string path, string projectName, ItemSectionGroup itemSectionGroup)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertItemSectionGroup, con);

				// Title,Synopsis,DateCreated

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = itemSectionGroup.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = itemSectionGroup.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = itemSectionGroup.DateCreated
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				itemSectionGroup.Id = (int)LastRowID64;
			}

			return itemSectionGroup.Id;
		}

		public List<ItemSectionGroup> GetItemSectionGroups(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<ItemSectionGroup> itemSectionGroups = new List<ItemSectionGroup>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemSectionGroups, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ItemSectionGroup itemSectionGroup = new ItemSectionGroup()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString()
						};

						itemSectionGroups.Add(itemSectionGroup);
					}
				}
			}
			return itemSectionGroups;
		}

		public int AddCustomField(string path, string projectName, CustomField customField)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertCustomField, con);

				// ProjectId,Title,Synopsis,DateCreated

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = customField.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = customField.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = customField.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = customField.DateCreated
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				customField.Id = (int)LastRowID64;
			}

			return customField.Id;
		}
		public List<CustomField> GetCustomFields(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<CustomField> customFields = new List<CustomField>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetCustomFields, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CustomField customField = new CustomField()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ProjectId = int.Parse(reader["ProjectId"].ToString())
						};

						customFields.Add(customField);
					}
				}
			}
			return customFields;
		}

		public int AddItemLabel(string path, string projectName, ItemLabel label)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertItemLabel, con);

				// Title,Synopsis,DateCreated,LabelColor

				 SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = label.Title
				 };
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = label.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = label.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@LabelColor",
					Value = label.LabelColor
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				label.Id = (int)LastRowID64;
			}

			return label.Id;
		}

		public ItemLabel GetItemLabel(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			ItemLabel itemLabel = new ItemLabel();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemLabel, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						itemLabel.Id = int.Parse(reader["Id"].ToString());
						itemLabel.Title = reader["Title"].ToString();
						itemLabel.Synopsis = reader["Synopsis"].ToString();
						itemLabel.DateCreated = reader["DateCreated"].ToString();
						itemLabel.LabelColor = int.Parse(reader["LabelColor"].ToString());
					}
				}
			}
			return itemLabel;
		}

		public List<ItemLabel> GetItemLabels(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<ItemLabel> itemLabels = new List<ItemLabel>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemLabels, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ItemLabel itemLabel = new ItemLabel()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							LabelColor = int.Parse(reader["LabelColor"].ToString())
						};

						itemLabels.Add(itemLabel);
					}
				}
			}
			return itemLabels;
		}

		public int AddItemSection(string path, string projectName, ItemSection section)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertItemSection, con);

				// ItemSectionGroupId,Title,Synopsis,DateCreated,SectionColor

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ItemSectionGroupId",
					Value = section.ItemSectionGroupId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = section.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = section.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = section.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SectionColor",
					Value = section.SectionColor
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				section.Id = (int)LastRowID64;
			}

			return section.Id;
		}

		public ItemSection GetItemSection(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			ItemSection itemSection = new ItemSection();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemSection, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						itemSection.Id = int.Parse(reader["Id"].ToString());
						itemSection.Title = reader["Title"].ToString();
						itemSection.Synopsis = reader["Synopsis"].ToString();
						itemSection.DateCreated = reader["DateCreated"].ToString();
						itemSection.ItemSectionGroupId = int.Parse(reader["ItemSectionGroupId"].ToString());
					}
				}
			}
			return itemSection;
		}

		public List<ItemSection> GetItemSectionsByGroup(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<ItemSection> itemSections = new List<ItemSection>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemSectionsByGroup, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@groupid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ItemSection itemSection = new ItemSection()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ItemSectionGroupId = int.Parse(reader["ItemSectionGroupId"].ToString())
						};

						itemSections.Add(itemSection);
					}
				}
			}
			return itemSections;
		}

		public List<ItemSection> GetItemSections(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<ItemSection> itemSections = new List<ItemSection>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemSections, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ItemSection itemSection = new ItemSection()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ItemSectionGroupId = int.Parse(reader["ItemSectionGroupId"].ToString())
						};

						itemSections.Add(itemSection);
					}
				}
			}
			return itemSections;
		}

		public int AddItemStatus(string path, string projectName, ItemStatus status)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertItemStatus, con);

				// ProjectId,Title,Synopsis,DateCreated,StatusColor

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = status.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = status.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = status.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = status.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@StatusColor",
					Value = status.StatusColor
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				status.Id = (int)LastRowID64;
			}

			return status.Id;
		}

		public ItemStatus GetItemStatus(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			ItemStatus itemStatus = new ItemStatus();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemStatus, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						itemStatus.Id = int.Parse(reader["Id"].ToString());
						itemStatus.Title = reader["Title"].ToString();
						itemStatus.Synopsis = reader["Synopsis"].ToString();
						itemStatus.DateCreated = reader["DateCreated"].ToString();
						itemStatus.ProjectId = int.Parse(reader["ProjectId"].ToString());
						itemStatus.StatusColor = int.Parse(reader["StatusColor"].ToString());
					}
				}
			}
			return itemStatus;
		}

		public List<ItemStatus> GetItemStatuses(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<ItemStatus> itemStatuses = new List<ItemStatus>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetItemStatuses, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ItemStatus itemStatus = new ItemStatus()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							StatusColor = int.Parse(reader["StatusColor"].ToString())
						};

						itemStatuses.Add(itemStatus);
					}
				}
			}
			return itemStatuses;
		}

		public int AddMetaDataItem(string path, string projectName, MetaDataItem metaDataItem)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.InsertMetaDataItem, con);

				// Title,Synopsis,DateCreated,LabelColor

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = metaDataItem.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = metaDataItem.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = metaDataItem.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@MetaDataType",
					Value = metaDataItem.MetaDataType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@CustomFormat",
					Value = metaDataItem.CustomFormat
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@ForeColor",
					Value = metaDataItem.ForeColor
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@BackColor",
					Value = metaDataItem.BackColor
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				metaDataItem.Id = (int)LastRowID64;
			}

			return metaDataItem.Id;
		}

		public List<LinkedMetaDataItem> GetLinkedMetaDataItems(string path, string projectName, int objectType, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<LinkedMetaDataItem> metaDataItems = new List<LinkedMetaDataItem>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLinkedMetaDataItems, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@objecttype",
					Value = objectType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						LinkedMetaDataItem metaDataItem = new LinkedMetaDataItem()
						{
							Id = int.Parse(reader["Id"].ToString()),
							MetaDataItemId = int.Parse(reader["MetaDataItemId"].ToString()),
							ObjectType = int.Parse(reader["ObjectType"].ToString()),
							ObjectId = int.Parse(reader["ObjectId"].ToString()),
							ForeColor = int.Parse(reader["ForeColor"].ToString()),
							BackColor = int.Parse(reader["BackColor"].ToString())
						};

						metaDataItems.Add(metaDataItem);
					}
				}
			}
			return metaDataItems;
		}

		public List<Note> GetNotes(string path, string projectName, int objectType, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Note> notes = new List<Note>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetNotesByObjectType, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@objecttype",
					Value = objectType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Note note = new Note()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ObjectType = int.Parse(reader["ObjectType"].ToString()),
							ObjectId = int.Parse(reader["ObjectId"].ToString()),
							ReminderDate = reader["ReminderDate"].ToString()
						};

						notes.Add(note);
					}
				}
			}
			return notes;
		}

		public List<Revision> GetRevisions(string path, string projectName, int objectType, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<Revision> revisions = new List<Revision>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetRevisionsByObjectType, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@objecttype",
					Value = objectType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Revision revision = new Revision()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							ObjectType = int.Parse(reader["ObjectType"].ToString()),
							ObjectId = int.Parse(reader["ObjectId"].ToString()),
							RevisionDate = reader["RevisionDate"].ToString(),
						};

						revisions.Add(revision);
					}
				}
			}
			return revisions;
		}

		public List<WritingSession> GetWritingSessions(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<WritingSession> writingSessions = new List<WritingSession>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetWritingSessions, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						WritingSession writingSession = new WritingSession()
						{
							ProjectId = int.Parse(reader["ProjectId"].ToString()),
							StartDate = reader["StartDate"].ToString(),
							EndDate = reader["EndDate"].ToString(),
							WordCountTarget = int.Parse(reader["WordCountTarget"].ToString()),
							WordCount = int.Parse(reader["WordCount"].ToString())
						};

						writingSessions.Add(writingSession);
					}
				}
			}
			return writingSessions;
		}

		public GroupSection GetGroupSection(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			GroupSection groupSection = new GroupSection();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetGroupSection, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						groupSection.Id = int.Parse(reader["Id"].ToString());
						groupSection.Title = reader["Title"].ToString();
						groupSection.Synopsis = reader["Synopsis"].ToString();
						groupSection.DateCreated = reader["DateCreated"].ToString();
					}
				}
			}
			return groupSection;
		}

		public GroupItem GetGroupItem(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			GroupItem groupItem = new GroupItem();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetGroupItem, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						groupItem.Id = int.Parse(reader["Id"].ToString());
						groupItem.Title = reader["Title"].ToString();
						groupItem.Synopsis = reader["Synopsis"].ToString();
						groupItem.DateCreated = reader["DateCreated"].ToString();
						groupItem.GroupSectionId = int.Parse(reader["GroupSectionId"].ToString());
					}
				}
			}
			return groupItem;
		}

		public List<MetaDataItem> GetMetaDataItems(string path, string projectName)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<MetaDataItem> metaDataItems = new List<MetaDataItem>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetMetaDataItems, con);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						MetaDataItem metaDataItem = new MetaDataItem()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							MetaDataType = byte.Parse(reader["MetaDataType"].ToString()),
							CustomFormat = reader["CustomFormat"].ToString(),
							ForeColor = int.Parse(reader["ForeColor"].ToString()),
							BackColor = int.Parse(reader["BackColor"].ToString())
						};

						metaDataItems.Add(metaDataItem);
					}
				}
			}
			return metaDataItems;
		}
	}
}
