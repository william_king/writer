﻿using Finisar.SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Writer.Entities;

namespace Writer.DataAccess.DataAccess
{
	public class ProjectRepository
	{
		private string _projectDbName;
		private string _projectName;

		public bool CreateProjectDataStructure(string projectName)
		{
			try
			{
				_projectName = projectName;
				_projectDbName = _projectName + ".wrp";
				if (!File.Exists(_projectDbName))
				{
					SQLiteConnection sqlConn = new SQLiteConnection($"Data Source={_projectDbName};Version=3;New=True;Compress=True;");
					sqlConn.Open();

					string sql = @"CREATE TABLE tblChapter(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								SortOrder			INTEGER
							);";
					SQLiteCommand cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblCharacter(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								Name TEXT,
								Surname TEXT,
								Image TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblCustomField(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblGroupSection(
								Id INTEGER PRIMARY KEY,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblLinkedGroupSection(
								Id INTEGER PRIMARY KEY ,
								ObjectType INTEGER,
								ObjectId		INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblGroupItem(
								Id INTEGER PRIMARY KEY ,
								GroupSectionId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblLinkedGroupItem(
								Id INTEGER PRIMARY KEY ,
								LinkedGroupSectionId INTEGER,
								GroupItemId INTEGER,
								ObjectType INTEGER,
								ObjectId		INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblItemLabel(
								Id INTEGER PRIMARY KEY,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								LabelColor INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblItemSection(
								Id INTEGER PRIMARY KEY,
								ItemSectionGroupId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								SectionColor INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblItemSectionGroup(
								Id INTEGER PRIMARY KEY,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblItemStatus(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								StatusColor INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblLocation(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								Image TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblMetaDataItem(
								Id INTEGER PRIMARY KEY ,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								MetaDataType INTEGER,
								CustomFormat TEXT,
								ForeColor INTEGER,
								BackColor INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblLinkedMetaDataItem(
								Id INTEGER PRIMARY KEY ,
								MetaDataItemId INTEGER,
								ObjectType INTEGER,
								ObjectId		INTEGER
								ForeColor INTEGER,
								BackColor INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblNote(
								Id INTEGER PRIMARY KEY ,
								ObjectType INTEGER,
								ObjectId		INTEGER
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								ReminderDate TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblPlot(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblProjectLayout(
								Id INTEGER PRIMARY KEY ,
								Version INTEGER,
								Title			TEXT		NOT NULL,
								Author	TEXT,
								Premise TEXT,
								DateCreated				TEXT,
								PlannedCompleteDate			TEXT,
								CompleteDate		TEXT,
								Synopsis			TEXT,
								LastFullBackup			TEXT,
								WordTarget			INTEGER,
								IsCompleted		string,
								WordTargetStartDate		TEXT,
								WordTargetEndDate		TEXT,
								WordCountStart		INTEGER,
								LastViewedSceneId			INTEGER,
								DesktopPath TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblResearchItem(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								DateCreated				TEXT,
								Url TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblRevision(
								Id INTEGER PRIMARY KEY ,
								ObjectType INTEGER,
								ObjectId		INTEGER
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblScene(
								Id INTEGER PRIMARY KEY ,
								ChapterId INTEGER,
								PlotId INTEGER,
								LocationId INTEGER,
								StoryEventId INTEGER,
								ItemSectionId INTEGER,
								ItemStatusId INTEGER,
								ItemLabelId INTEGER,
								PointOfView INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								SceneContent			TEXT,
								PlannedWordCount			TEXT,
								StartDate				TEXT,
								StartTime				TEXT,
								EndDate				TEXT,
								EndTime				TEXT,
								SortOrder INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblSceneCharacter(
								Id INTEGER PRIMARY KEY ,
								SceneId INTEGER,
								CharacterId INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblSceneEvent(
								Id INTEGER PRIMARY KEY ,
								SceneId INTEGER,
								StoryEventId INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblSceneObject(
								Id INTEGER PRIMARY KEY ,
								SceneId INTEGER,
								StoryObjectId INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblSceneTheme(
								Id INTEGER PRIMARY KEY ,
								SceneId INTEGER,
								ThemeId INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblStoryEvent(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblStoryObject(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT,
								Image TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblTarget(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								DateCreated				TEXT,
								OutlineStart				TEXT,
								OutlineEnd				TEXT,
								DraftStart				TEXT,
								DraftEnd				TEXT,
								FirstEditStart				TEXT,
								FirstEditEnd				TEXT,
								SecondEditStart				TEXT,
								SecondEditEnd				TEXT,
								FinalEditStart				TEXT,
								FinalEditEnd				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblTheme(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								Title			TEXT		NOT NULL,
								Synopsis			TEXT,
								DateCreated				TEXT
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();

					sql = @"CREATE TABLE tblWritingSession(
								Id INTEGER PRIMARY KEY ,
								ProjectId INTEGER,
								DateCreated				TEXT,
								StartDate				TEXT,
								EndDate				TEXT,
								WordCountTarget INTEGER,
								WordCount INTEGER
							);";
					cmd = new SQLiteCommand(sql, sqlConn);
					cmd.ExecuteNonQuery();


					sqlConn.Close();
				}

				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public void AddProject(ProjectLayout projectLayout)
		{
			_projectDbName = projectLayout.DesktopPath + @"\" + projectLayout.Title + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				SQLiteCommand command = new SQLiteCommand(ProjectSprocs.InsertProjectLayout, con);

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@Version",
					Value = projectLayout.Version
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Title",
					Value = projectLayout.Title
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Author",
					Value = projectLayout.Author
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Premise",
					Value = projectLayout.Premise
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = projectLayout.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@PlannedCompleteDate",
					Value = projectLayout.PlannedCompleteDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@CompleteDate",
					Value = projectLayout.CompleteDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@Synopsis",
					Value = projectLayout.Synopsis
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@LastFullBackup",
					Value = projectLayout.LastFullBackup
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@WordTarget",
					Value = projectLayout.WordTarget
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@IsCompleted",
					Value = projectLayout.IsCompleted
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@WordTargetStartDate",
					Value = projectLayout.WordTargetStartDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@WordTargetEndDate",
					Value = projectLayout.WordTargetEndDate
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@WordCountStart",
					Value = projectLayout.WordCountStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@LastViewedSceneId",
					Value = projectLayout.LastViewedSceneId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DesktopPath",
					Value = projectLayout.DesktopPath
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				projectLayout.Id = (int)LastRowID64;
			}
		}

		public void UpdateProject(ProjectLayout projectLayout)
		{
			_projectDbName = projectLayout.DesktopPath + @"\" + projectLayout.Title + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				SQLiteCommand cmd = new SQLiteCommand();
				con.Open();
				cmd.Connection = con;
				cmd.CommandText = $"insert into tblProjectLayout(Title) values ('{projectLayout.Title}')";
				cmd.ExecuteNonQuery();
			}
		}

		public ProjectLayout GetProject(string path, string title)
		{
			_projectDbName = path + @"\" + title + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetProjectLayout, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@title",
					Value = title
				};
				command.Parameters.Add(param);

				ProjectLayout projectLayout = null;

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						projectLayout = new ProjectLayout()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Version = int.Parse(reader["Version"].ToString()),
							Title = reader["Title"].ToString(),
							Author = reader["Author"].ToString(),
							Premise = reader["Premise"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							PlannedCompleteDate = reader["PlannedCompleteDate"].ToString(),
							CompleteDate = reader["CompleteDate"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							LastFullBackup = reader["LastFullBackup"].ToString(),
							WordTarget = int.Parse(reader["WordTarget"].ToString()),
							IsCompleted = reader["IsCompleted"].ToString(),
							WordTargetStartDate = reader["WordTargetStartDate"].ToString(),
							WordTargetEndDate = reader["WordTargetEndDate"].ToString(),
							WordCountStart = int.Parse(reader["WordCountStart"].ToString()),
							LastViewedSceneId = int.Parse(reader["LastViewedSceneId"].ToString()),
							DesktopPath = reader["DesktopPath"].ToString()
						};
					}
				}
				return projectLayout;
			}
		}



		//public ItemSection GetItemSection(string path, string projectName, int id)
		//{
		//	_projectDbName = path + @"\" + projectName + ".wrp";

		//	ItemSection itemSection = new ItemSection();

		//	using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
		//	{
		//		con.Open();

		//		var command = new SQLiteCommand(ProjectSprocs.GetItemSection, con);
		//		SQLiteParameter param = new SQLiteParameter()
		//		{
		//			ParameterName = "@id",
		//			Value = id
		//		};
		//		command.Parameters.Add(param);

		//		using (var reader = command.ExecuteReader())
		//		{
		//			if (reader.Read())
		//			{
		//				itemSection.Id = int.Parse(reader["Id"].ToString());
		//				itemSection.Title = reader["Title"].ToString();
		//				itemSection.Synopsis = reader["Synopsis"].ToString();
		//				itemSection.DateCreated = reader["DateCreated"].ToString();
		//				itemSection.ItemSectionGroupId = int.Parse(reader["ItemSectionGroupId"].ToString());
		//			}
		//		}
		//	}
		//	return itemSection;
		//}

		//public ItemStatus GetItemStatus(string path, string projectName, int id)
		//{
		//	_projectDbName = path + @"\" + projectName + ".wrp";

		//	ItemStatus itemStatus = new ItemStatus();

		//	using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
		//	{
		//		con.Open();

		//		var command = new SQLiteCommand(ProjectSprocs.GetItemStatus, con);
		//		SQLiteParameter param = new SQLiteParameter()
		//		{
		//			ParameterName = "@id",
		//			Value = id
		//		};
		//		command.Parameters.Add(param);

		//		using (var reader = command.ExecuteReader())
		//		{
		//			if (reader.Read())
		//			{
		//				itemStatus.Id = int.Parse(reader["Id"].ToString());
		//				itemStatus.Title = reader["Title"].ToString();
		//				itemStatus.Synopsis = reader["Synopsis"].ToString();
		//				itemStatus.DateCreated = reader["DateCreated"].ToString();
		//				itemStatus.ProjectId = int.Parse(reader["ProjectId"].ToString());
		//				itemStatus.StatusColor = int.Parse(reader["StatusColor"].ToString());
		//			}
		//		}
		//	}
		//	return itemStatus;
		//}

		//public ItemLabel GetItemLabel(string path, string projectName, int id)
		//{
		//	_projectDbName = path + @"\" + projectName + ".wrp";

		//	ItemLabel itemLabel = new ItemLabel();

		//	using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
		//	{
		//		con.Open();

		//		var command = new SQLiteCommand(ProjectSprocs.GetItemLabel, con);
		//		SQLiteParameter param = new SQLiteParameter()
		//		{
		//			ParameterName = "@id",
		//			Value = id
		//		};
		//		command.Parameters.Add(param);

		//		using (var reader = command.ExecuteReader())
		//		{
		//			if (reader.Read())
		//			{
		//				itemLabel.Id = int.Parse(reader["Id"].ToString());
		//				itemLabel.Title = reader["Title"].ToString();
		//				itemLabel.Synopsis = reader["Synopsis"].ToString();
		//				itemLabel.DateCreated = reader["DateCreated"].ToString();
		//				itemLabel.LabelColor = int.Parse(reader["LabelColor"].ToString());
		//			}
		//		}
		//	}
		//	return itemLabel;
		//}

		

		public LinkedGroupSection GetLinkedGroupSection(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			LinkedGroupSection linkedGroupSection = new LinkedGroupSection();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLinkedGroupSection, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						linkedGroupSection.Id = int.Parse(reader["Id"].ToString());
						linkedGroupSection.GroupSectionId = int.Parse(reader["GroupSectionId"].ToString());
						linkedGroupSection.ObjectType = int.Parse(reader["ObjectType"].ToString());
						linkedGroupSection.ObjectId = int.Parse(reader["ObjectId"].ToString());
					}
				}
			}
			return linkedGroupSection;
		}

		public LinkedGroupItem GetLinkedGroupItem(string path, string projectName, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			LinkedGroupItem linkedGroupItem = new LinkedGroupItem();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLinkedGroupItem, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@id",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						linkedGroupItem.Id = int.Parse(reader["Id"].ToString());
						linkedGroupItem.LinkedGroupSectionId = int.Parse(reader["GroupSectionId"].ToString());
						linkedGroupItem.GroupItemId = int.Parse(reader["GroupItemId"].ToString());
						linkedGroupItem.ObjectType = int.Parse(reader["ObjectType"].ToString());
						linkedGroupItem.ObjectId = int.Parse(reader["ObjectId"].ToString());
					}
				}
			}
			return linkedGroupItem;
		}

		public List<LinkedGroupSection> GetLinkedGroupSections(string path, string projectName, int objectType, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<LinkedGroupSection> linkedGroupSections = new List<LinkedGroupSection>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLinkedGroupSections, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@objecttype",
					Value = objectType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						LinkedGroupSection linkedGroupSection = new LinkedGroupSection()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							GroupSectionId = int.Parse(reader["GroupSectionId"].ToString()),
							ObjectType = int.Parse(reader["ObjectType"].ToString()),
							ObjectId = int.Parse(reader["ObjectId"].ToString())
						};

						linkedGroupSections.Add(linkedGroupSection);
					}
				}
			}
			return linkedGroupSections;
		}

		public List<LinkedGroupItem> GetLinkedGroupItems(string path, string projectName, int objectType, int id)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			List<LinkedGroupItem> linkedGroupItems = new List<LinkedGroupItem>();

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				var command = new SQLiteCommand(ProjectSprocs.GetLinkedGroupItems, con);
				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@objecttype",
					Value = objectType
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@objectid",
					Value = id
				};
				command.Parameters.Add(param);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						LinkedGroupItem linkedGroupItem = new LinkedGroupItem()
						{
							Id = int.Parse(reader["Id"].ToString()),
							Title = reader["Title"].ToString(),
							Synopsis = reader["Synopsis"].ToString(),
							DateCreated = reader["DateCreated"].ToString(),
							LinkedGroupSectionId = int.Parse(reader["LinkedGroupSectionId"].ToString()),
							GroupItemId = int.Parse(reader["GroupItemId"].ToString()),
							ObjectType = int.Parse(reader["ObjectType"].ToString()),
							ObjectId = int.Parse(reader["ObjectId"].ToString())
						};

						linkedGroupItems.Add(linkedGroupItem);
					}
				}
			}
			return linkedGroupItems;
		}

		public int AddTarget(string path, string projectName, Target target)
		{
			_projectDbName = path + @"\" + projectName + ".wrp";

			using (SQLiteConnection con = new SQLiteConnection("Data Source=" + _projectDbName + ";Version=3;"))
			{
				con.Open();

				SQLiteCommand command = new SQLiteCommand(ProjectSprocs.InsertTarget, con);

				SQLiteParameter param = new SQLiteParameter()
				{
					ParameterName = "@ProjectId",
					Value = target.ProjectId
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DateCreated",
					Value = target.DateCreated
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@OutlineStart",
					Value = target.OutlineStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@OutlineEnd",
					Value = target.OutlineEnd
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DraftStart",
					Value = target.DraftStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@DraftEnd",
					Value = target.DraftEnd
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@FirstEditStart",
					Value = target.FirstEditStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@FirstEditEnd",
					Value = target.FirstEditEnd
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SecondEditStart",
					Value = target.SecondEditStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@SecondEditEnd",
					Value = target.SecondEditEnd
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@FinalEditStart",
					Value = target.FinalEditStart
				};
				command.Parameters.Add(param);

				param = new SQLiteParameter()
				{
					ParameterName = "@FinalEditEnd",
					Value = target.FinalEditEnd
				};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();

				command.CommandText = "select last_insert_rowid()";
				Int64 LastRowID64 = (Int64)command.ExecuteScalar();

				target.Id = (int)LastRowID64;
			}

			return target.Id;
		}
	}
}