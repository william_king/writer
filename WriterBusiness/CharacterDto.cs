﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class CharacterDto : BaseDataObjectDto
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Image { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public CharacterDto()
		{
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddCharacter()
		{

		}

		public void AddCharacter(string path, string title, CharacterDto characterDto)
		{
			Character  character = new Character()
			{
				Title = characterDto.Title,
				Synopsis = characterDto.Synopsis,
				DateCreated = characterDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				Name = characterDto.Name,
				Surname = characterDto.Surname,
				Image = characterDto.Image
			};

			characterDto.Id = new StoryRepository().AddCharacter(path, title, character);

		}

		public void UpdateCharacter()
		{

		}

		public void DeleteCharacter()
		{

		}

		public void CopyCharacter()
		{

		}

		public List<CharacterDto> LoadCharacters(string path, string title)
		{
			List<CharacterDto> characterDtos = new List<CharacterDto>();

			List<Character> characters = new StoryRepository().GetCharacters(path, title);
			if (characters.Count > 0)
			{
				foreach (Character character in characters)
				{
					CharacterDto characterDto = new CharacterDto()
					{
						Id = character.Id,
						Title = character.Title,
						Synopsis = character.Synopsis,
						DateCreated = DateTime.Parse(character.DateCreated).ToLocalTime(),
						IsNew = false

					};

					characterDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Character, characterDto.Id);

					characterDtos.Add(characterDto);
				}
			}

			return characterDtos;
		}

		public List<CharacterDto> LoadSceneCharacters(string path, string title, int id)
		{
			List<CharacterDto> characterDtos = new List<CharacterDto>();

			List<SceneCharacter> sceneCharacters = new StoryRepository().GetSceneCharacters(path, title, id);
			if (sceneCharacters.Count > 0)
			{
				foreach (SceneCharacter sceneCharacter in sceneCharacters)
				{
					CharacterDto characterDto = new CharacterDto()
					{
						Id = sceneCharacter.CharacterId,
						Title = sceneCharacter.Title,
						Synopsis = sceneCharacter.Synopsis,
						DateCreated = DateTime.Parse(sceneCharacter.DateCreated).ToLocalTime(),
						IsNew = false

					};

					Character character = new StoryRepository().GetCharacter(path, title, characterDto.Id);
					characterDto.Name = character.Name;
					characterDto.Surname = character.Surname;
					characterDto.Image = character.Image;

					characterDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Character, characterDto.Id);
					characterDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Character, characterDto.Id);

					characterDtos.Add(characterDto);
				}
			}

			return characterDtos;
		}

		public CharacterDto GetCharacterForScene(string path, string title, int sceneId, int characterId)
		{
			SceneCharacter sceneCharacter = new StoryRepository().GetCharacterForScene(path, title, sceneId, characterId);
			if (sceneCharacter != null)
			{
				CharacterDto sceneCharacterDto = new CharacterDto()
				{
					Id = sceneCharacter.CharacterId,
					Title = sceneCharacter.Title,
					Synopsis = sceneCharacter.Synopsis,
					DateCreated = DateTime.Parse(sceneCharacter.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, sceneCharacter.CharacterId),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Character, sceneCharacter.CharacterId),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Character, sceneCharacter.CharacterId),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Character, sceneCharacter.CharacterId),
					IsNew = false

				};
				return sceneCharacterDto;
			}

			return null;
		}

		public CharacterDto GetCharacter(string path, string title, int id)
		{
			Character character = new StoryRepository().GetCharacter(path, title, id);
			if (character != null)
			{
				CharacterDto characterDto = new CharacterDto()
				{
					Id = character.Id,
					Title = character.Title,
					Synopsis = character.Synopsis,
					DateCreated = DateTime.Parse(character.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, character.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Character, character.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Character, character.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Character, character.Id),
					IsNew = false

				};
				return characterDto;
			}

			return null;
		}

		public void AddGroupSection()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<GroupSectionDto> GetGroupSections()
		{
			return new List<GroupSectionDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
