﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ItemLabelDto : BaseDataObjectDto
	{
		public Color LabelColor { get; set; }

		public ItemLabelDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddItemLabel(string path, string title, ItemLabelDto itemLabelDto)
		{
			ItemLabel itemLabel = new ItemLabel()
			{
				Title = itemLabelDto.Title,
				Synopsis = itemLabelDto.Synopsis,
				DateCreated = itemLabelDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				LabelColor = itemLabelDto.LabelColor.ToArgb()
			};

			itemLabelDto.Id = new SupplementaryRepository().AddItemLabel(path, title, itemLabel);
		}

		public void UpdateItemLabel()
		{

		}

		public void DeleteItemLabel()
		{

		}

		public void CopyItemLabel()
		{

		}

		public List<ItemLabelDto> LoadItemLabels(string path, string title)
		{
			List<ItemLabelDto> itemLabelDtos = new List<ItemLabelDto>();

			List<ItemLabel> itemLabels = new SupplementaryRepository().GetItemLabels(path, title);
			if (itemLabels.Count > 0)
			{
				foreach (ItemLabel itemLabel in itemLabels)
				{
					ItemLabelDto itemLabelDto = new ItemLabelDto()
					{
						Id = itemLabel.Id,
						Title = itemLabel.Title,
						Synopsis = itemLabel.Synopsis,
						DateCreated = DateTime.Parse(itemLabel.DateCreated).ToLocalTime(),
						LabelColor = Color.FromArgb(itemLabel.LabelColor),
						IsNew = false
					};

					itemLabelDtos.Add(itemLabelDto);
				}
			}

			return itemLabelDtos;
		}

		public ItemLabelDto GetItemLabel(string path, string title, int id)
		{
			ItemLabel itemLabel = new SupplementaryRepository().GetItemLabel(path, title, id);
			if (itemLabel != null)
			{
				ItemLabelDto itemLabelDto = new ItemLabelDto()
				{
					Id = itemLabel.Id,
					Title = itemLabel.Title,
					Synopsis = itemLabel.Synopsis,
					DateCreated = DateTime.Parse(itemLabel.DateCreated).ToLocalTime(),
					LabelColor = Color.FromArgb(itemLabel.LabelColor),
					IsNew = false

				};
				return itemLabelDto;
			}

			return null;
		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
