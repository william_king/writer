﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class StoryEventDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public StoryEventDto()
		{
			//GroupSections = GetGroupSections();
			//MetaDataItems = GetMetaDataItems();
			//Notes = GetNotes();
			//Revisions = GetRevisions();
		}
		public void AddStoryEvent()
		{

		}
		public void AddStoryEvent(string path, string title, StoryEventDto storyEventDto)
		{
			StoryEvent storyEvent = new StoryEvent()
			{
				ProjectId = storyEventDto.ProjectId,
				Title = storyEventDto.Title,
				Synopsis = storyEventDto.Synopsis,
				DateCreated = storyEventDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			storyEventDto.Id = new StoryRepository().AddStoryEvent(path, title, storyEvent);

		}


		public void UpdateStoryEvent()
		{

		}

		public void DeleteStoryEvent()
		{

		}

		public void CopyStoryEvent()
		{

		}

		public List<StoryEventDto> LoadEvents(string path, string title)
		{
			List<StoryEventDto> storyEventDtos = new List<StoryEventDto>();

			List<StoryEvent> storyEvents = new StoryRepository().GetStoryEvents(path, title);
			if (storyEvents.Count > 0)
			{
				foreach (StoryEvent storyEvent in storyEvents)
				{
					StoryEventDto storyEventDto = new StoryEventDto()
					{
						Id = storyEvent.Id,
						Title = storyEvent.Title,
						Synopsis = storyEvent.Synopsis,
						DateCreated = DateTime.Parse(storyEvent.DateCreated).ToLocalTime(),
						IsNew = false

					};

					storyEventDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);

					storyEventDtos.Add(storyEventDto);
				}
			}

			return storyEventDtos;
		}

		public List<StoryEventDto> LoadStoryEvents(string path, string title, int id)
		{
			List<StoryEventDto> storyEventDtos = new List<StoryEventDto>();

			List<SceneEvent> sceneEvents = new StoryRepository().GetSceneEvents(path, title, id);
			if (sceneEvents.Count > 0)
			{
				foreach (SceneEvent sceneEvent in sceneEvents)
				{
					StoryEventDto storyEventDto = new StoryEventDto()
					{
						Id = sceneEvent.StoryEventId,
						Title = sceneEvent.Title,
						Synopsis = sceneEvent.Synopsis,
						DateCreated = DateTime.Parse(sceneEvent.DateCreated).ToLocalTime(),
						IsNew = false

					};

					storyEventDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);
					storyEventDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryEvent, storyEventDto.Id);

					storyEventDtos.Add(storyEventDto);
				}
			}

			return storyEventDtos;
		}

		public StoryEventDto GetStoryEventForScene(string path, string title, int sceneId, int eventId)
		{
			SceneEvent sceneEvent = new StoryRepository().GetEventForScene(path, title, sceneId, eventId);
			if (sceneEvent != null)
			{
				StoryEventDto storyEventDto = new StoryEventDto()
				{
					Id = sceneEvent.StoryEventId,
					Title = sceneEvent.Title,
					Synopsis = sceneEvent.Synopsis,
					DateCreated = DateTime.Parse(sceneEvent.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryEvent, sceneEvent.StoryEventId),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryEvent, sceneEvent.StoryEventId),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryEvent, sceneEvent.StoryEventId),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryEvent, sceneEvent.StoryEventId),
					IsNew = false

				};
				return storyEventDto;
			}

			return null;
		}

		public StoryEventDto GetStoryEvent(string path, string title, int id)
		{
			StoryEvent storyEvent = new StoryRepository().GetStoryEvent(path, title, id);
			if (storyEvent != null)
			{
				StoryEventDto storyEventDto = new StoryEventDto()
				{
					Id = storyEvent.Id,
					Title = storyEvent.Title,
					Synopsis = storyEvent.Synopsis,
					DateCreated = DateTime.Parse(storyEvent.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryEvent, storyEvent.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryEvent, storyEvent.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryEvent, storyEvent.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryEvent, storyEvent.Id),
					IsNew = false

				};
				return storyEventDto;
			}

			return null;
		}

		public void AddGroupSection()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<GroupSectionDto> GetGroupSections()
		{
			return new List<GroupSectionDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
