﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class GroupItemDto : BaseDataObjectDto
	{
		public GroupSectionDto GroupSectionDto { get; set; }
		public GroupItemDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddGroupItem()
		{

		}

		public void UpdateGroupItem()
		{

		}

		public void DeleteGroupItem()
		{

		}

		public void CopyGroupItem()
		{

		}
		public GroupItemDto GetGroupItem(string path, string title, int id)
		{
			GroupItem groupItem = new SupplementaryRepository().GetGroupItem(path, title, id);
			if (groupItem != null)
			{
				GroupItemDto groupItemDto = new GroupItemDto()
				{
					Id = groupItem.Id,
					Title = groupItem.Title,
					Synopsis = groupItem.Synopsis,
					DateCreated = DateTime.Parse(groupItem.DateCreated).ToLocalTime(),
					GroupSectionDto = new GroupSectionDto().GetGroupSection(path, title, groupItem.GroupSectionId),
					IsNew = false

				};
				return groupItemDto;
			}

			return null;
		}

		public void AddNote()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
