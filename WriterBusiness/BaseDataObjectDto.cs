﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Business
{
	[Serializable]
	public class BaseDataObjectDto
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Synopsis { get; set; }
		public DateTime DateCreated { get; set; }
		public bool IsNew { get; set; }
		public List<MetaDataItemDto> MetaDataItems { get; set; }
		public List<NoteDto> Notes { get; set; }
		public List<RevisionDto> Revisions { get; set; }

		public BaseDataObjectDto()
		{
			MetaDataItems = new List<MetaDataItemDto>();
			Notes = new List<NoteDto>();
			Revisions = new List<RevisionDto>();
		}
	}
}
