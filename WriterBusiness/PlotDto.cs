﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class PlotDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public PlotDto()
		{
			//GroupSections = GetGroupSections();
			//MetaDataItems = GetMetaDataItems();
			//Notes = GetNotes();
			//Revisions = GetRevisions();
		}
		public void AddPlot(string path, string title, PlotDto plotDto)
		{
			Plot plot = new Plot()
			{
				ProjectId = plotDto.ProjectId,
				Title = plotDto.Title,
				Synopsis = plotDto.Synopsis,
				DateCreated = plotDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			plotDto.Id = new StoryRepository().AddPlot(path, title, plot);

		}

		public void UpdatePlot()
		{

		}

		public void DeletePlot()
		{

		}

		public void CopyPlot()
		{

		}

		public List<PlotDto> LoadPlots(string path, string title)
		{
			List<PlotDto> plotDtos = new List<PlotDto>();

			List<Plot> plots = new StoryRepository().GetPlots(path, title);
			if (plots.Count > 0)
			{
				foreach (Plot plot in plots)
				{
					PlotDto plotDto = new PlotDto()
					{
						Id = plot.Id,
						Title = plot.Title,
						Synopsis = plot.Synopsis,
						DateCreated = DateTime.Parse(plot.DateCreated).ToLocalTime(),
						ProjectId = plot.ProjectId,
						IsNew = false

					};

					plotDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryEvent, plotDto.Id);
					plotDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryEvent, plotDto.Id);
					plotDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryEvent, plotDto.Id);
					plotDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryEvent, plotDto.Id);

					plotDtos.Add(plotDto);
				}
			}

			return plotDtos;
		}

		public void AddMetaDataItem()
		{

		}

		public PlotDto GetPlot(string path, string title, int id)
		{
			Plot plot = new StoryRepository().GetPlot(path, title, id);
			if (plot != null)
			{
				PlotDto plotDto = new PlotDto()
				{
					Id = plot.Id,
					Title = plot.Title ?? string.Empty,
					Synopsis = plot.Synopsis ?? string.Empty,
					DateCreated = DateTime.Parse(plot.DateCreated).ToLocalTime(),
					ProjectId = plot.ProjectId,
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Plot, plot.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Plot, plot.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Plot, plot.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Plot, plot.Id),
					IsNew = false

				};

				//LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Plot, plot.Id),
				//LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Plot, plot.Id),
				//Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Plot, plot.Id),
				//Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Plot, plot.Id),

				return plotDto;
			}

			return null;
		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<GroupSectionDto> GetGroupSections()
		{
			return new List<GroupSectionDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
