﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class CustomFieldDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public CustomFieldDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddCustomField(string path, string title, CustomFieldDto customFieldDto)
		{
			CustomField target = new CustomField()
			{
				ProjectId = customFieldDto.ProjectId,
				Title = customFieldDto.Title,
				Synopsis = customFieldDto.Synopsis,
				DateCreated = customFieldDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			customFieldDto.Id = new  SupplementaryRepository().AddCustomField(path, title, target);
		}

		public void UpdateCustomField()
		{

		}

		public void DeleteCustomField()
		{

		}

		public void CopyCustomField()
		{

		}

		public List<CustomFieldDto> LoadCustomFields(string path, string title)
		{
			List<CustomFieldDto> customFieldDtos = new List<CustomFieldDto>();

			List<CustomField> customFields = new SupplementaryRepository().GetCustomFields(path, title);
			if (customFields.Count > 0)
			{
				foreach (CustomField customField in customFields)
				{
					CustomFieldDto customFieldDto = new CustomFieldDto()
					{
						Id = customField.Id,
						Title = customField.Title,
						Synopsis = customField.Synopsis,
						DateCreated = DateTime.Parse(customField.DateCreated).ToLocalTime(),
						ProjectId = customField.ProjectId,
						IsNew = false
					};

					customFieldDtos.Add(customFieldDto);
				}
			}

			return customFieldDtos;
		}
		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
