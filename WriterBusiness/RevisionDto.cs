﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class RevisionDto : BaseDataObjectDto
	{
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }
		public DateTime RevisionDate { get; set; }
		public RevisionDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}
		public string Text { get; set; }

		public void AddRevision()
		{

		}

		public void UpdateRevision()
		{

		}

		public void DeleteRevision()
		{

		}

		public void CopyRevision()
		{

		}

		public List<RevisionDto> LoadRevisions(string path, string title, ObjectTypes objectType, int id)
		{
			List<RevisionDto> revisionDtos = new List<RevisionDto>();

			List<Revision> revisions = new SupplementaryRepository().GetRevisions(path, title, (int)objectType, id);
			if (revisions.Count > 0)
			{
				foreach (Revision revision in revisions)
				{
					RevisionDto revisionDto = new RevisionDto()
					{
						Id = revision.Id,
						Title = revision.Title,
						Synopsis = revision.Synopsis,
						DateCreated = DateTime.Parse(revision.DateCreated).ToLocalTime(),
						RevisionDate = DateTime.Parse(revision.RevisionDate).ToLocalTime(),
						ObjectType = revision.ObjectType,
						ObjectId = revision.ObjectId,
						IsNew = false
					};

					revisionDtos.Add(revisionDto);
				}
			}

			return revisionDtos;
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
