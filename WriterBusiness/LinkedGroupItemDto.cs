﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class LinkedGroupItemDto : BaseDataObjectDto
	{
		public LinkedGroupSectionDto LinkedGroupSection { get; set; }
		public GroupItemDto GroupItem { get; set; }
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }

		public List<LinkedGroupItemDto> GetLinkedGroupSections(string path, string title, ObjectTypes objectType, int id)
		{
			List<LinkedGroupItemDto> linkedGroupItemDtos = new List<LinkedGroupItemDto>();

			List<LinkedGroupItem> linkedGroupItems = new ProjectRepository().GetLinkedGroupItems(path, title, (int)objectType, id);
			if (linkedGroupItems.Count > 0)
			{
				foreach (LinkedGroupItem linkedGroupItem in linkedGroupItems)
				{
					LinkedGroupItemDto linkedGroupItemDto = new LinkedGroupItemDto()
					{
						Id = linkedGroupItem.Id,
						GroupItem = new GroupItemDto().GetGroupItem(path, title, linkedGroupItem.GroupItemId),
						LinkedGroupSection = new LinkedGroupSectionDto().GetLinkedGroupSection(path, title, linkedGroupItem.LinkedGroupSectionId),
						ObjectType = linkedGroupItem.ObjectType,
						ObjectId = linkedGroupItem.ObjectId,
						IsNew = false

					};
					linkedGroupItemDtos.Add(linkedGroupItemDto);
				}
			}

			return linkedGroupItemDtos;
		}
	}
}
