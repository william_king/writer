﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Business
{
	public enum ObjectTypes
	{
		Project = 0,
		Chapter = 1,
		Scene = 2,
		Character = 3,
		Location = 4,
		StoryEvent = 5,
		StoryObject = 6,
		Plot = 7,
		Theme = 8
	}
}
