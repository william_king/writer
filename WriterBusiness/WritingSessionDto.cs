﻿using System;
using System.Collections.Generic;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class WritingSessionDto
	{
		public int ProjectId { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int WordCountTarget { get; set; }
		public int WordCount { get; set; }
		public int Duration { get { return CalculateDuration(); } }

		public List<WritingSessionDto> LoadWritingSessions(string path, string title)
		{
			List<WritingSessionDto> writingSessionDtos = new List<WritingSessionDto>();

			List<WritingSession> writingSessions = new SupplementaryRepository().GetWritingSessions(path, title);
			if (writingSessions.Count > 0)
			{
				foreach (WritingSession writingSession in writingSessions)
				{
					WritingSessionDto writingSessionDto = new WritingSessionDto()
					{
						StartDate = DateTime.Parse(writingSession.StartDate).ToLocalTime(),
						EndDate = DateTime.Parse(writingSession.EndDate).ToLocalTime(),
						WordCountTarget = writingSession.WordCountTarget,
						WordCount = writingSession.WordCount,
						ProjectId = writingSession.ProjectId
					};

					writingSessionDtos.Add(writingSessionDto);
				}
			}

			return writingSessionDtos;
		}

		public void StartWritingSession()
		{
		}

		public void StopWritingSession()
		{
		}

		public void PauseWritingSession()
		{
		}

		private int CalculateDuration()
		{
			int duration = (int)(EndDate.TimeOfDay - StartDate.TimeOfDay).TotalSeconds;

			return duration;
		}

		public void SaveWritingSession()
		{
		}
	}
}