﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ProjectLayoutDto
	{
		public int Id { get; set; }
		public int Version { get; set; }
		public string Title { get; set; }
		public string Author { get; set; }
		public string Premise { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime PlannedCompleteDate { get; set; }
		public DateTime CompleteDate { get; set; }
		public string Synopsis { get; set; }
		public DateTime LastFullBackup { get; set; }
		public int WordTarget { get; set; }
		public bool IsCompleted { get; set; }
		public DateTime WordTargetStartDate { get; set; }
		public DateTime WordTargetEndDate { get; set; }
		public int WordCountStart { get; set; }
		public int LastViewedSceneId { get; set; }
		public string DesktopPath { get; set; }
		public bool IsNew { get; set; }
		public int TotalWordCount { get { return CalculateTotalWordCount(); } }
		public int ChapterWordCount { get { return Chapters.Count; } }
		public List<ChapterDto> Chapters { get; set; }
		public List<LocationDto> Locations { get; set; }
		public List<CharacterDto> Characters { get; set; }
		public List<StoryEventDto> StoryEvents { get; set; }
		public List<StoryObjectDto> StoryObjects { get; set; }
		public List<CustomFieldDto> CustomFields { get; set; }
		public List<MetaDataItemDto> MetaDataItems { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItems { get; set; }
		public List<ItemSectionGroupDto> ItemSectionGroups { get; set; }
		public List<ItemLabelDto> ItemLabels { get; set; }
		public List<ItemSectionDto> ItemSections { get; set; }
		public List<ItemStatusDto> ItemStatuses { get; set; }
		public List<NoteDto> Notes { get; set; }
		public List<RevisionDto> Revisions { get; set; }
		public List<WritingSessionDto> WritingSessions { get; set; }
		public List<TargetDto> Targets { get; set; }
		public List<ThemeDto> Themes { get; set; }
		public List<PlotDto> Plots { get; set; }

		public ProjectLayoutDto()
		{
			Chapters = new List<ChapterDto>();
			Locations = new List<LocationDto>();
			Characters = new List<CharacterDto>();
			StoryEvents = new List<StoryEventDto>();
			StoryObjects = new List<StoryObjectDto>();
			ItemLabels = new List<ItemLabelDto>();
			ItemSections = new List<ItemSectionDto>();
			ItemStatuses = new List<ItemStatusDto>();
			MetaDataItems = new List<MetaDataItemDto>();
			Targets = new List<TargetDto>();
			CustomFields = new List<CustomFieldDto>();
			ItemSectionGroups = new List<ItemSectionGroupDto>();
			Themes = new List<ThemeDto>();
			Plots = new List<PlotDto>();
		}

		private void CreateDefaultItemSecionGroup()
		{
			var i = new ItemSectionGroupDto()
			{
				Title = "Default",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now
			};
			ItemSectionGroups.Add(i);
		}

		private void CreateDefaultItemLabels()
		{
			var i = new ItemLabelDto()
			{
				Title = "Red",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Red
			};
			ItemLabels.Add(i);

			i = new ItemLabelDto()
			{
				Title = "Orange",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Orange
			};
			ItemLabels.Add(i);

			i = new ItemLabelDto()
			{
				Title = "Yellow",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Yellow
			};
			ItemLabels.Add(i);

			i = new ItemLabelDto()
			{
				Title = "Green",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Green
			};
			ItemLabels.Add(i);

			i = new ItemLabelDto()
			{
				Title = "Blue",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Blue
			};
			ItemLabels.Add(i);

			i = new ItemLabelDto()
			{
				Title = "Purple",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				LabelColor = Color.Purple
			};
			ItemLabels.Add(i);
		}

		private void CreateDefaultItemSections()
		{
			var i = new ItemSectionDto()
			{
				Title = "Scene",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				SectionColor = Color.Black

			};
			ItemSections.Add(i);

			i = new ItemSectionDto()
			{
				Title = "Heading",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				SectionColor = Color.Black

			};
			ItemSections.Add(i);

			i = new ItemSectionDto()
			{
				Title = "N/A",
				Synopsis = string.Empty,
				DateCreated = DateTime.Now,
				SectionColor = Color.Black

			};
			ItemSections.Add(i);
		}

		private void CreateDefaultItemStatuses()
		{
			var i = new ItemStatusDto()
			{
				Title = "To Do",
				Synopsis = "",
				DateCreated = DateTime.Now,
				StatusColor = Color.Black
			};
			ItemStatuses.Add(i);

			i = new ItemStatusDto()
			{
				Title = "First Draft",
				Synopsis = "",
				DateCreated = DateTime.Now,
				StatusColor = Color.Black
			};
			ItemStatuses.Add(i);

			i = new ItemStatusDto()
			{
				Title = "Revised Draft",
				Synopsis = "",
				DateCreated = DateTime.Now,
				StatusColor = Color.Black
			};
			ItemStatuses.Add(i);

			i = new ItemStatusDto()
			{
				Title = "Final Draft",
				Synopsis = "",
				DateCreated = DateTime.Now,
				StatusColor = Color.Black
			};
			ItemStatuses.Add(i);

			i = new ItemStatusDto()
			{
				Title = "Completed",
				Synopsis = "",
				DateCreated = DateTime.Now,
				StatusColor = Color.Black
			};
			ItemStatuses.Add(i);
		}

		public bool CreateProject()
		{
			ProjectLayout projectLayout = PrepareEntity();
			IsNew = true;

			CreateDefaultItemSecionGroup();
			CreateDefaultItemLabels();
			CreateDefaultItemSections();
			CreateDefaultItemStatuses();

			var projectDataAccess = new ProjectRepository();
			if (projectDataAccess.CreateProjectDataStructure(projectLayout.DesktopPath + @"\" + projectLayout.Title))
			{
				projectDataAccess.AddProject(projectLayout);

				if (Themes.Count > 0)
				{
					foreach (ThemeDto themeDto in Themes)
					{
						themeDto.ProjectId = projectLayout.Id;
						themeDto.AddTheme(projectLayout.DesktopPath, projectLayout.Title, themeDto);
					}
				}

				if (Plots.Count > 0)
				{
					foreach (PlotDto plotDto in Plots)
					{
						plotDto.ProjectId = projectLayout.Id;
						plotDto.AddPlot(projectLayout.DesktopPath, projectLayout.Title, plotDto);
					}
				}

				if (Chapters.Count > 0)
				{
					foreach (ChapterDto chapterDto in Chapters)
					{
						chapterDto.ProjectId = projectLayout.Id;
						chapterDto.SortOrder = Chapters.Count;
						chapterDto.AddChapter(projectLayout.DesktopPath, projectLayout.Title, chapterDto);
					}
				}

				if (Locations.Count > 0)
				{
					foreach (LocationDto locationDto in Locations)
					{
						locationDto.ProjectId = projectLayout.Id;
						locationDto.AddLocation(projectLayout.DesktopPath, projectLayout.Title, locationDto);
					}
				}

				if (Characters.Count > 0)
				{
					foreach (CharacterDto characterDto in Characters)
					{
						characterDto.AddCharacter(projectLayout.DesktopPath, projectLayout.Title, characterDto);
					}
				}

				if (StoryEvents.Count > 0)
				{
					foreach (StoryEventDto storyEventDto in StoryEvents)
					{
						storyEventDto.ProjectId = projectLayout.Id;
						storyEventDto.AddStoryEvent(projectLayout.DesktopPath, projectLayout.Title, storyEventDto);
					}
				}

				if (StoryObjects.Count > 0)
				{
					foreach (StoryObjectDto storyObjectDto in StoryObjects)
					{
						storyObjectDto.ProjectId = projectLayout.Id;
						storyObjectDto.AddStoryObject(projectLayout.DesktopPath, projectLayout.Title, storyObjectDto);
					}
				}

				if (ItemLabels.Count > 0)
				{
					foreach (ItemLabelDto itemLabelDto in ItemLabels)
					{
						itemLabelDto.AddItemLabel(projectLayout.DesktopPath, projectLayout.Title, itemLabelDto);
					}
				}

				if (ItemSectionGroups.Count > 0)
				{
					foreach (ItemSectionGroupDto itemSectionGroupDto in ItemSectionGroups)
					{
						itemSectionGroupDto.AddItemSectionGroup(projectLayout.DesktopPath, projectLayout.Title, itemSectionGroupDto);
					}
				}

				if (ItemSections.Count > 0)
				{
					foreach (ItemSectionDto itemSectionDto in ItemSections)
					{
						itemSectionDto.ItemSectionGroupId = ItemSectionGroups[0].Id;
						itemSectionDto.AddItemSection(projectLayout.DesktopPath, projectLayout.Title, itemSectionDto);
					}
				}

				if (ItemStatuses.Count > 0)
				{
					foreach (ItemStatusDto itemStatusDto in ItemStatuses)
					{
						itemStatusDto.ProjectId = projectLayout.Id;
						itemStatusDto.AddItemStatus(projectLayout.DesktopPath, projectLayout.Title, itemStatusDto);
					}
				}

				if (MetaDataItems.Count > 0)
				{
					foreach (MetaDataItemDto metaDataItemDto in MetaDataItems)
					{
						metaDataItemDto.AddMetaDataItem(projectLayout.DesktopPath, projectLayout.Title, metaDataItemDto);
					}
				}

				if (CustomFields.Count > 0)
				{
					foreach (CustomFieldDto customFieldDto in CustomFields)
					{
						customFieldDto.ProjectId = projectLayout.Id;
						customFieldDto.AddCustomField(projectLayout.DesktopPath, projectLayout.Title, customFieldDto);
					}
				}

				if (Targets.Count > 0)
				{
					foreach (TargetDto targetDto in Targets)
					{
						targetDto.ProjectId = projectLayout.Id;
						targetDto.AddTarget(projectLayout.DesktopPath, projectLayout.Title, targetDto);
					}
				}

				IsNew = false; //set project's new status to false to prevent it from being added again
				return true;
			}
			else
				return false;
		}

		public void UpdateProject()
		{
			ProjectLayout projectLayout = PrepareEntity();
			var projectDataAccess = new ProjectRepository();
			projectDataAccess.UpdateProject(projectLayout);
		}

		public void DeleteProject()
		{
			string projectDbName = Title + ".wrp";
			if (File.Exists(projectDbName))
				File.Delete(projectDbName);
		}

		public ProjectLayoutDto CopyProject(string title)
		{
			ProjectLayoutDto projectLayoutDto = new ProjectLayoutDto()
			{
				IsNew = true,
				Version = Version,
				Title = title,
				Author = Author,
				Premise = Premise,
				DateCreated = DateTime.Now,
				PlannedCompleteDate = PlannedCompleteDate,
				CompleteDate = CompleteDate,
				Synopsis = Synopsis,
				LastFullBackup = LastFullBackup,
				WordTarget = WordTarget,
				IsCompleted = IsCompleted,
				WordTargetStartDate = WordTargetStartDate,
				WordTargetEndDate = WordTargetEndDate,
				WordCountStart = WordCountStart,
				LastViewedSceneId = LastViewedSceneId,
				Chapters = Chapters,
				CustomFields = CustomFields,
				MetaDataItems = MetaDataItems
			};

			return projectLayoutDto;
		}


		public void GetProject()
		{
			var projectDataAccess = new ProjectRepository();
			var projectLayout = projectDataAccess.GetProject(DesktopPath, Title);
			if (projectLayout != null)
			{
				Id = projectLayout.Id;
				Version = projectLayout.Version;
				Title = projectLayout.Title;
				Author = projectLayout.Author;
				Premise = projectLayout.Premise;
				DateCreated = DateTime.Parse(projectLayout.DateCreated).ToLocalTime();
				PlannedCompleteDate = DateTime.Parse(projectLayout.PlannedCompleteDate).ToLocalTime();
				CompleteDate = DateTime.Parse(projectLayout.CompleteDate).ToLocalTime();
				Synopsis = projectLayout.Synopsis;
				LastFullBackup = DateTime.Parse(projectLayout.LastFullBackup).ToLocalTime();
				WordTarget = projectLayout.WordTarget;
				IsCompleted = Convert.ToBoolean(projectLayout.IsCompleted);
				WordTargetStartDate = DateTime.Parse(projectLayout.WordTargetStartDate).ToLocalTime();
				WordTargetEndDate = DateTime.Parse(projectLayout.WordTargetEndDate).ToLocalTime();
				WordCountStart = projectLayout.WordCountStart;
				LastViewedSceneId = projectLayout.LastViewedSceneId;
				DesktopPath = projectLayout.DesktopPath;
				IsNew = false;

				Chapters = new ChapterDto().LoadChapters(DesktopPath, Title);
				Locations = new LocationDto().LoadLocations(DesktopPath, Title, Id);
				Characters = new CharacterDto().LoadCharacters(DesktopPath, Title);
				StoryObjects = new StoryObjectDto().LoadStoryObjects(DesktopPath, Title);
				StoryEvents = new StoryEventDto().LoadEvents(DesktopPath, Title);


				CustomFields = new CustomFieldDto().LoadCustomFields(DesktopPath, Title);
				MetaDataItems = new MetaDataItemDto().LoadMetaDataItems(DesktopPath, Title);
				LinkedMetaDataItems = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(DesktopPath, Title, 0, Id);
				Notes = new NoteDto().LoadNotes(DesktopPath, Title, 0, Id);
				Revisions = new RevisionDto().LoadRevisions(DesktopPath, Title, 0, Id);
				WritingSessions = new WritingSessionDto().LoadWritingSessions(DesktopPath, Title);

				ItemSectionGroups = new ItemSectionGroupDto().LoadItemSectionGroups(DesktopPath, Title);
				ItemSections = new ItemSectionDto().LoadItemSections(DesktopPath, Title);
				ItemLabels = new ItemLabelDto().LoadItemLabels(DesktopPath, Title);
				ItemStatuses = new ItemStatusDto().LoadItemStatuses(DesktopPath, Title);

				Themes = new ThemeDto().LoadThemes(DesktopPath, Title);
				Plots = new PlotDto().LoadPlots(DesktopPath, Title);

				IsNew = false; //set project's new status to false to prevent it from being added again
			}

		}

		public void AddCustomField()
		{
		}

		public void UpdateCustomField()
		{
		}

		public void DeleteCustomField()
		{
		}

		public void CopyCustomField()
		{
		}

		public void AddMetaDataItem()
		{
		}

		public void AddNote()
		{
		}

		public void AddRevision()
		{
		}

		private int CalculateTotalWordCount()
		{
			return 0;
		}

		private List<ChapterDto> GetChapters(bool empty)
		{
			if (empty)
				return new List<ChapterDto>();
			else
				return new List<ChapterDto>();
		}

		private List<CustomFieldDto> GetCustomFields(bool empty)
		{
			if (empty)
				return new List<CustomFieldDto>();
			else
				return new List<CustomFieldDto>();
		}

		private List<WritingSessionDto> GetWritingSessions(bool empty)
		{
			if (empty)
				return new List<WritingSessionDto>();
			else
				return new List<WritingSessionDto>();
		}

		private List<LinkedMetaDataItemDto> GetLinkedMetaDataItems(bool empty)
		{
			if (empty)
				return new List<LinkedMetaDataItemDto>();
			else
				return new List<LinkedMetaDataItemDto>();
		}
		private List<NoteDto> GetNotes(bool empty)
		{
			if (empty)
				return new List<NoteDto>();
			else
				return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions(bool empty)
		{
			if (empty)
				return new List<RevisionDto>();
			else
				return new List<RevisionDto>();
		}

		private ProjectLayout PrepareEntity()
		{
			ProjectLayout projectLayout = new ProjectLayout()
			{
				Version = Version,
				Title = Title,
				Author = Author,
				Premise = Premise,
				DateCreated = DateCreated.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				PlannedCompleteDate = PlannedCompleteDate.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				CompleteDate = CompleteDate.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				Synopsis = Synopsis,
				LastFullBackup = LastFullBackup.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				WordTarget = WordTarget,
				IsCompleted = IsCompleted.ToString(),
				WordTargetStartDate = WordTargetStartDate.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				WordTargetEndDate = WordTargetEndDate.ToUniversalTime().ToString("yyy-MM-dd HH:mm:ss"),
				WordCountStart = WordCountStart,
				LastViewedSceneId = LastViewedSceneId,
				DesktopPath = DesktopPath
			};

			return projectLayout;

		}
	}
}