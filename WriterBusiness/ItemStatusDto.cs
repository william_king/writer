﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ItemStatusDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public Color StatusColor { get; set; }

		public ItemStatusDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddItemStatus(string path, string title, ItemStatusDto itemStatusDto)
		{
			ItemStatus itemStatus = new ItemStatus()
			{
				Title = itemStatusDto.Title,
				Synopsis = itemStatusDto.Synopsis,
				DateCreated = itemStatusDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				StatusColor = itemStatusDto.StatusColor.ToArgb()
			};

			itemStatusDto.Id = new SupplementaryRepository().AddItemStatus(path, title, itemStatus);
		}

		public void UpdateItemStatus()
		{

		}

		public void DeleteItemStatus()
		{

		}

		public void CopyItemStatus()
		{

		}

		public List<ItemStatusDto> LoadItemStatuses(string path, string title)
		{
			List<ItemStatusDto> itemStatusDtos = new List<ItemStatusDto>();

			List<ItemStatus> itemStatuses = new SupplementaryRepository().GetItemStatuses(path, title);
			if (itemStatuses.Count > 0)
			{
				foreach (ItemStatus itemStatus in itemStatuses)
				{
					ItemStatusDto itemStatusDto = new ItemStatusDto()
					{
						Id = itemStatus.Id,
						Title = itemStatus.Title,
						Synopsis = itemStatus.Synopsis,
						DateCreated = DateTime.Parse(itemStatus.DateCreated).ToLocalTime(),
						StatusColor = Color.FromArgb(itemStatus.StatusColor),
						IsNew = false
					};

					itemStatusDtos.Add(itemStatusDto);
				}
			}

			return itemStatusDtos;
		}

		public ItemStatusDto GetItemStatus(string path, string title, int id)
		{
			ItemStatus itemStatus = new SupplementaryRepository().GetItemStatus(path, title, id);
			if (itemStatus != null)
			{
				ItemStatusDto itemstatusDto = new ItemStatusDto()
				{
					Id = itemStatus.Id,
					Title = itemStatus.Title,
					Synopsis = itemStatus.Synopsis,
					DateCreated = DateTime.Parse(itemStatus.DateCreated).ToLocalTime(),
					ProjectId = itemStatus.ProjectId,
					StatusColor = Color.FromArgb(itemStatus.StatusColor),
					IsNew = false

				};
				return itemstatusDto;
			}

			return null;
		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
