﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ItemSectionDto : BaseDataObjectDto
	{
		public int ItemSectionGroupId { get; set; }
		public Color SectionColor { get; set; }


		public ItemSectionDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddItemSection(string path, string title, ItemSectionDto itemSectionDto)
		{
			ItemSection itemSection = new ItemSection()
			{
				ItemSectionGroupId = itemSectionDto.ItemSectionGroupId,
				Title = itemSectionDto.Title,
				Synopsis = itemSectionDto.Synopsis,
				DateCreated = itemSectionDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				SectionColor = itemSectionDto.SectionColor.ToArgb()
			};

			itemSectionDto.Id = new SupplementaryRepository().AddItemSection(path, title, itemSection);
		}

		public void UpdateItemSection()
		{

		}

		public void DeleteItemSection()
		{

		}

		public void CopyItemSection()
		{

		}

		public List<ItemSectionDto> LoadItemSections(string path, string title)
		{
			List<ItemSectionDto> itemSectionDtos = new List<ItemSectionDto>();

			List<ItemSection> itemSections = new SupplementaryRepository().GetItemSections(path, title);
			if (itemSections.Count > 0)
			{
				foreach (ItemSection itemSection in itemSections)
				{
					ItemSectionDto itemSectionDto = new ItemSectionDto()
					{
						Id = itemSection.Id,
						Title = itemSection.Title,
						Synopsis = itemSection.Synopsis,
						DateCreated = DateTime.Parse(itemSection.DateCreated).ToLocalTime(),
						ItemSectionGroupId = itemSection.ItemSectionGroupId,
						SectionColor = Color.FromArgb(itemSection.SectionColor),
						IsNew = false
					};

					itemSectionDtos.Add(itemSectionDto);
				}
			}

			return itemSectionDtos;
		}

		public List<ItemSectionDto> LoadItemSectionsByGroup(string path, string title, int id)
		{
			List<ItemSectionDto> itemSectionDtos = new List<ItemSectionDto>();

			List<ItemSection> itemSections = new SupplementaryRepository().GetItemSectionsByGroup(path, title, id);
			if (itemSections.Count > 0)
			{
				foreach (ItemSection itemSection in itemSections)
				{
					ItemSectionDto itemSectionDto = new ItemSectionDto()
					{
						Id = itemSection.Id,
						Title = itemSection.Title,
						Synopsis = itemSection.Synopsis,
						DateCreated = DateTime.Parse(itemSection.DateCreated).ToLocalTime(),
						ItemSectionGroupId = itemSection.ItemSectionGroupId,
						SectionColor = Color.FromArgb(itemSection.SectionColor),
						IsNew = false
					};

					itemSectionDtos.Add(itemSectionDto);
				}
			}

			return itemSectionDtos;
		}

		public ItemSectionDto GetItemSection(string path, string title, int id)
		{
			ItemSection itemSection = new SupplementaryRepository().GetItemSection(path, title, id);
			if (itemSection != null)
			{
				ItemSectionDto itemSectionDto = new ItemSectionDto()
				{
					Id = itemSection.Id,
					Title = itemSection.Title,
					Synopsis = itemSection.Synopsis,
					DateCreated = DateTime.Parse(itemSection.DateCreated).ToLocalTime(),
					ItemSectionGroupId = itemSection.ItemSectionGroupId,
					SectionColor = Color.FromArgb(itemSection.SectionColor),
					IsNew = false
				};
				return itemSectionDto;
			}

			return null;
		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
