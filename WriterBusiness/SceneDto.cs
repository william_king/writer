﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class SceneDto : BaseDataObjectDto
	{
		public int ChapterId { get; set; }
		public PlotDto ScenePlot { get; set; }
		public string SceneContent { get; set; }
		public int PlannedWordCount { get; set; }
		public int CurrentWordCount { get { return CalculateCurrentWordCount(); } }
		public int CharacterCount { get { return CalculateCurrentCharacterCount(); } }
		public int ParagraphCount { get { return CalculateCurrentParagraphCount(); } }
		public int SentenceCount { get { return CalculateCurrentSentenceCount(); } }
		public DateTime StartDate { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndDate { get; set; }
		public DateTime EndTime { get; set; }
		public int Duration { get { return CalculateDuration(); } }
		public LocationDto SceneLocation { get; set; }
		public ItemSectionDto Section { get; set; }
		public CharacterDto PointOfView { get; set; }
		public ItemStatusDto Status { get; set; }
		public ItemLabelDto Label { get; set; }
		public int SortOrder { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public List<ThemeDto> Themes { get; set; }
		public List<CharacterDto> Characters { get; set; }
		public List<StoryObjectDto> StoryObjects { get; set; }
		public List<StoryEventDto> StoryEvents { get; set; }

		public SceneDto()
		{
			LinkedGroupSections = GetLinkedGroupSections();
			Themes = GetThemes();
			Characters = GetCharacters();
			StoryObjects = GetStoryObjects();
			StoryEvents = GetStoryEvents();
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddScene(string path, string title, SceneDto sceneDto)
		{
			Scene scene = new Scene()
			{
				Title = sceneDto.Title,
				Synopsis = sceneDto.Synopsis,
				DateCreated = sceneDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				ChapterId = sceneDto.ChapterId,
				PlotId = sceneDto.ScenePlot?.Id ?? 0,
				LocationId = sceneDto.SceneLocation?.Id ?? 0,
				ItemSectionId = sceneDto.Section?.Id ?? 0,
				ItemStatusId = sceneDto.Status?.Id ?? 0,
				ItemLabelId= sceneDto.Label?.Id ?? 0,
				PointOfView = sceneDto.PointOfView?.Id ?? 0,
				SceneContent = sceneDto.SceneContent,
				PlannedWordCount = sceneDto.PlannedWordCount,
				StartDate = sceneDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				StartTime = sceneDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				EndDate = sceneDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				EndTime = sceneDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				SortOrder = sceneDto.SortOrder
			};

			sceneDto.Id = new StoryRepository().AddScene(path, title, scene);

		}

		public void UpdateScene()
		{

		}

		public void DeleteScene()
		{

		}

		public void CopyScene()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public List<SceneDto> LoadScenes(string path, string title, int id)
		{
			List<SceneDto> sceneDtos = new List<SceneDto>();

			List<Scene> scenes = new ChapterRepository().GetScenes(path, title, id);
			if (scenes.Count > 0)
			{
				foreach (Scene scene in scenes)
				{
					SceneDto sceneDto = new SceneDto()
					{
						Id = scene.Id,
						Title = scene.Title,
						Synopsis = scene.Synopsis,
						DateCreated = DateTime.Parse(scene.DateCreated).ToLocalTime(),
						ChapterId = scene.ChapterId,
						ScenePlot = new PlotDto().GetPlot(path, title, scene.PlotId),
						SceneLocation = new LocationDto().GetLocation(path, title, scene.LocationId),
						Section = new ItemSectionDto().GetItemSection(path, title, scene.ItemSectionId),
						Status = new ItemStatusDto().GetItemStatus(path, title, scene.ItemStatusId),
						Label = new ItemLabelDto().GetItemLabel(path, title, scene.ItemLabelId),
						PointOfView = new CharacterDto().GetCharacter(path, title, scene.PointOfView),
						SceneContent = scene.SceneContent,
						PlannedWordCount = scene.PlannedWordCount,
						StartDate = DateTime.Parse(scene.StartDate).ToLocalTime(),
						StartTime = DateTime.Parse(scene.StartTime).ToLocalTime(),
						EndDate = DateTime.Parse(scene.EndDate).ToLocalTime(),
						EndTime = DateTime.Parse(scene.EndTime).ToLocalTime(),
						SortOrder = scene.SortOrder,
						IsNew = false
					};

					sceneDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Scene, sceneDto.Id);
					sceneDto.Themes = new ThemeDto().LoadSceneThemes(path, title, sceneDto.Id);
					sceneDto.Characters = new CharacterDto().LoadSceneCharacters(path, title, sceneDto.Id);
					sceneDto.StoryObjects = new StoryObjectDto().LoadStoryObjects(path, title);
					sceneDto.StoryEvents = new StoryEventDto().LoadStoryEvents(path, title, sceneDto.Id);

					sceneDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Scene, sceneDto.Id);
					sceneDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Chapter, sceneDto.Id);
					sceneDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Chapter, sceneDto.Id);

					sceneDtos.Add(sceneDto);


				}
			}

			return sceneDtos;
		}

		public void AddRevision()
		{

		}

		private int CalculateCurrentWordCount()
		{
			return 0;
		}
		private int CalculateCurrentCharacterCount()
		{
			return 0;
		}
		private int CalculateCurrentSentenceCount()
		{
			return 0;
		}
		private int CalculateCurrentParagraphCount()
		{
			return 0;
		}
		private int CalculateDuration()
		{
			int duration = 0; // (int)(EndDate.TimeOfDay - StartDate.TimeOfDay).TotalSeconds;

			return duration;
		}

		private List<LinkedGroupSectionDto> GetLinkedGroupSections()
		{
			return new List<LinkedGroupSectionDto>();
		}
		private List<ThemeDto> GetThemes()
		{
			return new List<ThemeDto>();
		}
		private List<CharacterDto> GetCharacters()
		{
			return new List<CharacterDto>();
		}
		private List<StoryObjectDto> GetStoryObjects()
		{
			return new List<StoryObjectDto>();
		}

		private List<StoryEventDto> GetStoryEvents()
		{
			return new List<StoryEventDto>();
		}
		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}

	}
}
