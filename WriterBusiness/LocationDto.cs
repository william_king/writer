﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class LocationDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public string Image { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public LocationDto()
		{
			//GroupSections = GetGroupSections();
			//MetaDataItems = GetMetaDataItems();
			//Notes = GetNotes();
			//Revisions = GetRevisions();
		}

		public void AddLocation()
		{

		}

		public void AddLocation(string path, string title, LocationDto locationDto)
		{
			Location location = new Location()
			{
				ProjectId = locationDto.ProjectId,
				Title = locationDto.Title,
				Synopsis = locationDto.Synopsis,
				DateCreated = locationDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				Image = locationDto.Image
			};

			locationDto.Id = new StoryRepository().AddLocation(path, title, location);

		}

		public void UpdateLocation()
		{

		}

		public void DeleteLocation()
		{

		}

		public void CopyLocation()
		{

		}

		public List<LocationDto> LoadLocations(string path, string title, int id)
		{
			List<LocationDto> locationDtos = new List<LocationDto>();

			List<Location> locations = new StoryRepository().GetLocations(path, title, id);
			if (locations.Count > 0)
			{
				foreach (Location location in locations)
				{
					LocationDto chapterDto = new LocationDto()
					{
						Id = location.Id,
						Title = location.Title,
						Synopsis = location.Synopsis,
						DateCreated = DateTime.Parse(location.DateCreated).ToLocalTime(),
						ProjectId = location.ProjectId,
						Image = location.Image,
						IsNew = false
					};

					chapterDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Chapter, chapterDto.Id);
					chapterDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Chapter, chapterDto.Id);
					chapterDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Chapter, chapterDto.Id);

					locationDtos.Add(chapterDto);


				}
			}

			return locationDtos;
		}

		public LocationDto GetLocation(string path, string title, int id)
		{
			Location location = new StoryRepository().GetLocation(path, title, id);
			if (location != null)
			{
				LocationDto locationDto = new LocationDto()
				{
					Id = location.Id,
					Title = location.Title,
					Synopsis = location.Synopsis,
					DateCreated = DateTime.Parse(location.DateCreated).ToLocalTime(),
					ProjectId = location.ProjectId,
					Image = location.Image,
					IsNew = false,
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Location, location.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Location, location.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Location, location.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Location, location.Id)

			};
				return locationDto;
			}

			return null;
		}

		public void AddGroupSection()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<GroupSectionDto> GetGroupSections()
		{
			return new List<GroupSectionDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
