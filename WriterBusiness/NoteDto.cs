﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class NoteDto : BaseDataObjectDto
	{
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }
		public DateTime ReminderDate { get; set; }

		public NoteDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddNote()
		{

		}

		public void UpdateNote()
		{

		}

		public void DeleteNote()
		{

		}

		public void CopyNote()
		{

		}

		public List<NoteDto> LoadNotes(string path, string title, ObjectTypes objectType, int id)
		{
			List<NoteDto> noteDtos = new List<NoteDto>();

			List<Note> notes = new SupplementaryRepository().GetNotes(path, title, (int)objectType, id);
			if (notes.Count > 0)
			{
				foreach (Note note in notes)
				{
					NoteDto noteDto = new NoteDto()
					{
						Id = note.Id,
						Title = note.Title,
						Synopsis = note.Synopsis,
						DateCreated = DateTime.Parse(note.DateCreated).ToLocalTime(),
						ReminderDate = DateTime.Parse(note.ReminderDate).ToLocalTime(),
						ObjectType = note.ObjectType,
						ObjectId = note.ObjectId,
						IsNew = false
					};

					noteDtos.Add(noteDto);
				}
			}

			return noteDtos;
		}

		public void AddRevision()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
