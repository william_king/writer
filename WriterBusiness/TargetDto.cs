﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class TargetDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public DateTime OutlineStart { get; set; }
		public DateTime OutlineEnd { get; set; }
		public DateTime DraftStart { get; set; }
		public DateTime DraftEnd { get; set; }
		public DateTime FirstEditStart { get; set; }
		public DateTime FirstEditEnd { get; set; }
		public DateTime SecondEditStart { get; set; }
		public DateTime SecondEditEnd { get; set; }
		public DateTime FinalEditStart { get; set; }
		public DateTime FinalEditEnd { get; set; }
		public TargetDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}
		public void AddTarget(string path, string title, TargetDto targetDto)
		{
			Target target = new Target()
			{

				OutlineStart = targetDto.OutlineStart.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				OutlineEnd = targetDto.OutlineEnd.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				DraftStart = targetDto.DraftStart.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				DraftEnd = targetDto.DraftEnd.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				FirstEditStart = targetDto.FirstEditStart.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				FirstEditEnd = targetDto.FirstEditEnd.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				SecondEditStart = targetDto.SecondEditStart.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				SecondEditEnd = targetDto.SecondEditEnd.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				FinalEditStart = targetDto.FinalEditStart.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				FinalEditEnd = targetDto.FinalEditEnd.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				Synopsis = targetDto.Synopsis,
				DateCreated = targetDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			targetDto.Id = new ProjectRepository().AddTarget(path, title, target);
		}

		public void UpdateTheme()
		{

		}

		public void DeleteTheme()
		{

		}

		public void CopyTheme()
		{

		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
