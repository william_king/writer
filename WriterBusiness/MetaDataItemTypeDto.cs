﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Business
{
	public enum MetaDataItemTypeDto
	{
		Text,
		Numeric,
		Date,
		Time,
		List,
		Checkbox
	}
}
