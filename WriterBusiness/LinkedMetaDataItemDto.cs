﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class LinkedMetaDataItemDto : BaseDataObjectDto
	{
		public int MetaDataItemId { get; set; }
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }
		public Color ForeColor { get; set; }
		public Color BackColor { get; set; }

		public List<LinkedMetaDataItemDto> LoadLinkedMetaDataItems(string path, string title, ObjectTypes objectType, int id)
		{
			List<LinkedMetaDataItemDto> linkedMetaDataItemDtos = new List<LinkedMetaDataItemDto>();

			List<LinkedMetaDataItem> linkedMetaDataItems = new SupplementaryRepository().GetLinkedMetaDataItems(path, title, (int)objectType, id);
			if (linkedMetaDataItems.Count > 0)
			{
				foreach (LinkedMetaDataItem linkedMetaDataItem in linkedMetaDataItems)
				{
					LinkedMetaDataItemDto linkedMetaDataItemDto = new LinkedMetaDataItemDto()
					{
						Id = linkedMetaDataItem.Id,
						MetaDataItemId = linkedMetaDataItem.MetaDataItemId,
						ObjectType = linkedMetaDataItem.ObjectType,
						ObjectId = linkedMetaDataItem.ObjectId,
						IsNew = false
					};

					linkedMetaDataItemDtos.Add(linkedMetaDataItemDto);
				}
			}

			return linkedMetaDataItemDtos;
		}


	}
}
