﻿using System;
using System.Collections.Generic;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ThemeDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public ThemeDto()
		{
			//LinkedGroupSections = GetLinkedGroupSections();
			////MetaDataItems = GetLinkedMetaDataItems();
			//Notes = GetNotes();
			//Revisions = GetRevisions();
		}
		public void AddTheme(string path, string title, ThemeDto themeDto)
		{
			Theme theme = new Theme()
			{
				ProjectId = themeDto.ProjectId,
				Title = themeDto.Title,
				Synopsis = themeDto.Synopsis,
				DateCreated = themeDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			themeDto.Id = new StoryRepository().AddTheme(path, title, theme);

		}

		public void UpdateTheme()
		{
		}

		public void DeleteTheme()
		{
		}

		public void CopyTheme()
		{
		}


		public void AddMetaDataItem()
		{
		}

		public List<ThemeDto> LoadThemes(string path, string title)
		{
			List<ThemeDto> themeDtos = new List<ThemeDto>();

			List<Theme> themes = new StoryRepository().GetThemes(path, title);
			if (themes.Count > 0)
			{
				foreach (Theme theme in themes)
				{
					ThemeDto themeDto = new ThemeDto()
					{
						Id = theme.Id,
						Title = theme.Title,
						Synopsis = theme.Synopsis,
						DateCreated = DateTime.Parse(theme.DateCreated).ToLocalTime(),
						ProjectId = theme.ProjectId,
						IsNew = false

					};

					themeDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Theme, themeDto.Id);

					themeDtos.Add(themeDto);
				}
			}

			return themeDtos;
		}

		public List<ThemeDto> LoadSceneThemes(string path, string title, int id)
		{
			List<ThemeDto> themeDtos = new List<ThemeDto>();

			List<SceneTheme> sceneThemes = new StoryRepository().GetSceneThemes(path, title, id);
			if (sceneThemes.Count > 0)
			{
				foreach (SceneTheme sceneTheme in sceneThemes)
				{
					ThemeDto themeDto = new ThemeDto()
					{
						Id = sceneTheme.ThemeId,
						Title = sceneTheme.Title,
						Synopsis = sceneTheme.Synopsis,
						DateCreated = DateTime.Parse(sceneTheme.DateCreated).ToLocalTime(),
						IsNew = false

					};

					themeDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Theme, themeDto.Id);
					themeDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Theme, themeDto.Id);

					themeDtos.Add(themeDto);
				}
			}

			return themeDtos;
		}

		public ThemeDto GetThemeForScene(string path, string title, int sceneId, int themeId)
		{
			SceneTheme sceneTheme = new StoryRepository().GetThemeForScene(path, title, sceneId, themeId);
			if (sceneTheme != null)
			{
				ThemeDto themeDto = new ThemeDto()
				{
					Id = sceneTheme.ThemeId,
					Title = sceneTheme.Title,
					Synopsis = sceneTheme.Synopsis,
					DateCreated = DateTime.Parse(sceneTheme.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, sceneTheme.ThemeId),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Theme, sceneTheme.ThemeId),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Theme, sceneTheme.ThemeId),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Theme, sceneTheme.ThemeId),
					IsNew = false

				};
				return themeDto;
			}

			return null;
		}

		public ThemeDto GetTheme(string path, string title, int id)
		{
			Theme theme = new StoryRepository().GetTheme(path, title, id);
			if (theme != null)
			{
				ThemeDto themeDto = new ThemeDto()
				{
					Id = theme.Id,
					Title = theme.Title,
					Synopsis = theme.Synopsis,
					DateCreated = DateTime.Parse(theme.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.Theme, theme.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Theme, theme.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Theme, theme.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Theme, theme.Id),
					ProjectId = theme.ProjectId,
					IsNew = false

				};
				return themeDto;
			}

			return null;
		}

		public void AddNote()
		{
		}

		public void AddRevision()
		{
		}

		private List<LinkedGroupSectionDto> GetLinkedGroupSections()
		{
			return new List<LinkedGroupSectionDto>();
		}

		private List<LinkedMetaDataItemDto> GetLinkedMetaDataItems()
		{
			return new List<LinkedMetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}