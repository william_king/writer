﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class LinkedGroupSectionDto : BaseDataObjectDto
	{
		public GroupSectionDto GroupSection { get; set; }

		public int GroupSectionId { get; set; }
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }

		public List<LinkedGroupSectionDto> GetLinkedGroupSections(string path, string title, ObjectTypes objectType, int id)
		{
			List<LinkedGroupSectionDto> linkedGroupSectionDtos = new List<LinkedGroupSectionDto>();

			List<LinkedGroupSection> linkedGroupSections = new ProjectRepository().GetLinkedGroupSections(path, title, (int)objectType, id);
			if (linkedGroupSections.Count > 0)
			{
				foreach (LinkedGroupSection linkedGroupSection in linkedGroupSections)
				{
					LinkedGroupSectionDto linkedGroupSectionDto = new LinkedGroupSectionDto()
					{
						Id = linkedGroupSection.Id,
						GroupSectionId = linkedGroupSection.GroupSectionId,
						ObjectType = linkedGroupSection.ObjectType,
						ObjectId = linkedGroupSection.ObjectId,
						IsNew = false

					};
					linkedGroupSectionDtos.Add(linkedGroupSectionDto);
				}
			}			

			return linkedGroupSectionDtos;
		}

		public LinkedGroupSectionDto GetLinkedGroupSection(string path, string title, int id)
		{
			LinkedGroupSection linkedGroupSection = new ProjectRepository().GetLinkedGroupSection(path, title, id);
			if (linkedGroupSection != null)
			{
				LinkedGroupSectionDto linkedGroupSectionDto = new LinkedGroupSectionDto()
				{
					Id = linkedGroupSection.Id,
					GroupSectionId = linkedGroupSection.GroupSectionId,
					ObjectType = linkedGroupSection.ObjectType,
					ObjectId = linkedGroupSection.ObjectId,
					IsNew = false

				};
				return linkedGroupSectionDto;
			}

			return null;
		}
	}
}
