﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class StoryObjectDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public string Image { get; set; }
		public List<LinkedGroupSectionDto> LinkedGroupSections { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public StoryObjectDto()
		{
			//GroupSections = GetGroupSections();
			//MetaDataItems = GetMetaDataItems();
			//Notes = GetNotes();
			//Revisions = GetRevisions();
		}

		public void AddStoryObject()
		{

		}

		public void AddStoryObject(string path, string title, StoryObjectDto storyObjectDto)
		{
			StoryObject storyObject = new StoryObject()
			{
				ProjectId = storyObjectDto.ProjectId,
				Title = storyObjectDto.Title,
				Synopsis = storyObjectDto.Synopsis,
				DateCreated = storyObjectDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				Image = storyObjectDto.Image
			};

			storyObjectDto.Id = new StoryRepository().AddStoryObject(path, title, storyObject);

		}

		public void UpdateStoryObject()
		{

		}

		public void DeleteStoryObject()
		{

		}

		public void CopyStoryObject()
		{

		}

		public List<StoryObjectDto> LoadStoryObjects(string path, string title)
		{
			List<StoryObjectDto> storyObjectDtos = new List<StoryObjectDto>();

			List<StoryObject> storyObjects = new StoryRepository().GetStoryObjects(path, title);
			if (storyObjects.Count > 0)
			{
				foreach (StoryObject sceneObject in storyObjects)
				{
					StoryObjectDto storyObjectDto = new StoryObjectDto()
					{
						Id = sceneObject.Id,
						Title = sceneObject.Title,
						Synopsis = sceneObject.Synopsis,
						DateCreated = DateTime.Parse(sceneObject.DateCreated).ToLocalTime(),
						IsNew = false

					};

					storyObjectDto.LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryObject, storyObjectDto.Id);
					storyObjectDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryObject, storyObjectDto.Id);
					storyObjectDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryObject, storyObjectDto.Id);
					storyObjectDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryObject, storyObjectDto.Id);

					storyObjectDtos.Add(storyObjectDto);
				}
			}

			return storyObjectDtos;
		}

		public StoryObjectDto GetStoryObjectForScene(string path, string title, int sceneId, int objectId)
		{
			SceneObject sceneObject = new StoryRepository().GetObjectForScene(path, title, sceneId, objectId);
			if (sceneObject != null)
			{
				StoryObjectDto storyObjectDto = new StoryObjectDto()
				{
					Id = sceneObject.StoryObjectId,
					Title = sceneObject.Title,
					Synopsis = sceneObject.Synopsis,
					DateCreated = DateTime.Parse(sceneObject.DateCreated).ToLocalTime(),
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryObject, sceneObject.StoryObjectId),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryObject, sceneObject.StoryObjectId),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryObject, sceneObject.StoryObjectId),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryObject, sceneObject.StoryObjectId),
					IsNew = false

				};
				return storyObjectDto;
			}

			return null;
		}

		public StoryObjectDto GetStoryObject(string path, string title, int id)
		{
			StoryObject storyObject = new StoryRepository().GetStoryObject(path, title, id);
			if (storyObject != null)
			{
				StoryObjectDto StoryObjectDto = new StoryObjectDto()
				{
					Id = storyObject.Id,
					Title = storyObject.Title,
					Synopsis = storyObject.Synopsis,
					DateCreated = DateTime.Parse(storyObject.DateCreated).ToLocalTime(),
					Image = storyObject.Image,
					LinkedGroupSections = new LinkedGroupSectionDto().GetLinkedGroupSections(path, title, ObjectTypes.StoryObject, storyObject.Id),
					LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.StoryObject, storyObject.Id),
					Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.StoryObject, storyObject.Id),
					Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.StoryObject, storyObject.Id),
					IsNew = false

				};
				return StoryObjectDto;
			}

			return null;
		}

		public void AddGroupSection()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<GroupSectionDto> GetGroupSections()
		{
			return new List<GroupSectionDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
