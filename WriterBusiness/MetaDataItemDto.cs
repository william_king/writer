﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class MetaDataItemDto : BaseDataObjectDto
	{
		public MetaDataItemTypeDto MetaDataType { get; set; }
		public string CustomFormat { get; set; }
		public Color ForeColor { get; set; }
		public Color BackColor { get; set; }

		public MetaDataItemDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddMetaDataItem(string path, string title, MetaDataItemDto metaDataItemDto)
		{
			MetaDataItem metaDataItem = new MetaDataItem()
			{
				Title = metaDataItemDto.Title,
				Synopsis = metaDataItemDto.Synopsis,
				DateCreated = metaDataItemDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				MetaDataType = (byte)metaDataItemDto.MetaDataType,
				CustomFormat = metaDataItemDto.CustomFormat,
				ForeColor = metaDataItemDto.ForeColor.ToArgb(),
				BackColor = metaDataItemDto.BackColor.ToArgb()
			};

			metaDataItemDto.Id = new SupplementaryRepository().AddMetaDataItem(path, title, metaDataItem);
		}

		public void UpdateMetaDataItem()
		{

		}

		public void DeleteMetaDataItem()
		{

		}

		public void CopyMetaDataItem()
		{

		}

		public List<MetaDataItemDto> LoadMetaDataItems(string path, string title)
		{
			List<MetaDataItemDto> metaDataItemDtos = new List<MetaDataItemDto>();

			List<MetaDataItem> metaDataItems = new SupplementaryRepository().GetMetaDataItems(path, title);
			if (metaDataItems.Count > 0)
			{
				foreach (MetaDataItem metaDataItem in metaDataItems)
				{
					MetaDataItemDto metaDataItemDto = new MetaDataItemDto()
					{
						Id = metaDataItem.Id,
						Title = metaDataItem.Title,
						Synopsis = metaDataItem.Synopsis,
						DateCreated = Convert.ToDateTime(metaDataItem.DateCreated).ToLocalTime(),
						MetaDataType = (MetaDataItemTypeDto)Enum.Parse(typeof(MetaDataItemTypeDto), metaDataItem.MetaDataType.ToString()),
						CustomFormat = metaDataItem.CustomFormat,
						ForeColor = Color.FromArgb(metaDataItem.ForeColor),
						BackColor = Color.FromArgb(metaDataItem.BackColor),
						IsNew = false
					};

					metaDataItemDtos.Add(metaDataItemDto);
				}
			}

			return metaDataItemDtos;
		}

		public void AddNote()
		{
		}

		public void AddRevision()
		{
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
