﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class GroupSectionDto : BaseDataObjectDto
	{
		public List<GroupItemDto> GroupItems { get; set; }
		public GroupSectionDto()
		{
			GroupItems = GetGroupItems();
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddGroupSection()
		{

		}

		public void UpdateGroupSection()
		{

		}

		public void DeleteGroupSection()
		{

		}


		public void CopyGroupSection()
		{

		}

		public GroupSectionDto GetGroupSection(string path, string title, int id)
		{
			GroupSection groupSection = new SupplementaryRepository().GetGroupSection(path, title, id);
			if (groupSection != null)
			{
				GroupSectionDto groupSectionDto = new GroupSectionDto()
				{
					Id = groupSection.Id,
					Title = groupSection.Title,
					Synopsis = groupSection.Synopsis,
					DateCreated = DateTime.Parse(groupSection.DateCreated).ToLocalTime(),
					IsNew = false

				};
				return groupSectionDto;
			}

			return null;
		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		private List<GroupItemDto> GetGroupItems()
		{
			return new List<GroupItemDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
