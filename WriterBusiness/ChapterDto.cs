﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ChapterDto : BaseDataObjectDto
	{
		public int ProjectId { get; set; }
		public int SortOrder { get; set; }
		public List<SceneDto> Scenes { get; set; }
		public List<LinkedMetaDataItemDto> LinkedMetaDataItemDtos { get; set; }
		public int SceneCount { get { return Scenes.Count; } }

		public ChapterDto()
		{
			Scenes = GetScenes();
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
		}

		public void AddChapter(string path, string title, ChapterDto chapterDto)
		{
			Chapter chapter = new Chapter()
			{
				ProjectId = chapterDto.ProjectId,
				Title = chapterDto.Title,
				Synopsis = chapterDto.Synopsis,
				DateCreated = chapterDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"),
				SortOrder = chapterDto.SortOrder
			};

			chapterDto.Id = new ChapterRepository().AddChapter(path, title, chapter);
			if (Scenes.Count > 0)
			{
				foreach (SceneDto sceneDto in Scenes)
				{
					sceneDto.ChapterId = chapterDto.Id;
					if (sceneDto.IsNew)
						sceneDto.SortOrder = chapterDto.Scenes.Count;
					sceneDto.AddScene(path, title, sceneDto);
				}
			}

		}

		public void UpdateChapter()
		{

		}

		public void DeleteChapter()
		{

		}

		public void CopyChapter()
		{

		}

		public List<ChapterDto> LoadChapters(string path, string title)
		{
			List<ChapterDto> chapterDtos = new List<ChapterDto>();

			List<Chapter> chapters = new ChapterRepository().GetChapters(path, title);
			if (chapters.Count > 0)
			{
				foreach (Chapter chapter in chapters)
				{
					ChapterDto chapterDto = new ChapterDto()
					{
						Id = chapter.Id,
						Title = chapter.Title,
						Synopsis = chapter.Synopsis,
						DateCreated = DateTime.Parse(chapter.DateCreated).ToLocalTime(),
						ProjectId = chapter.ProjectId,
						SortOrder = chapter.SortOrder,
						IsNew = false
					};
					
					chapterDto.Scenes = new SceneDto().LoadScenes(path, title, chapterDto.Id);
					chapterDto.LinkedMetaDataItemDtos = new LinkedMetaDataItemDto().LoadLinkedMetaDataItems(path, title, ObjectTypes.Chapter, chapterDto.Id);
					chapterDto.Notes = new NoteDto().LoadNotes(path, title, ObjectTypes.Chapter, chapterDto.Id);
					chapterDto.Revisions = new RevisionDto().LoadRevisions(path, title, ObjectTypes.Chapter, chapterDto.Id);

					chapterDtos.Add(chapterDto);


				}
			}

			return chapterDtos;
		}


		public void AddScene()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}

		public void AddRevision()
		{

		}

		private List<SceneDto> GetScenes()
		{
			return new List<SceneDto>();
		}

		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
