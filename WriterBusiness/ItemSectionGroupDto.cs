﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Writer.DataAccess.DataAccess;
using Writer.Entities;

namespace Writer.Business
{
	[Serializable]
	public class ItemSectionGroupDto : BaseDataObjectDto
	{

		public List<ItemSectionDto> ItemSections { get; set; }

		public ItemSectionGroupDto()
		{
			MetaDataItems = GetMetaDataItems();
			Notes = GetNotes();
			Revisions = GetRevisions();
			ItemSections = new List<ItemSectionDto>();
		}

		public void AddItemSectionGroup(string path, string title, ItemSectionGroupDto itemSectionGroupDto)
		{
			ItemSectionGroup  itemSectionGroup = new ItemSectionGroup()
			{
				Title = itemSectionGroupDto.Title,
				Synopsis = itemSectionGroupDto.Synopsis,
				DateCreated = itemSectionGroupDto.DateCreated.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss")
			};

			itemSectionGroupDto.Id = new SupplementaryRepository().AddItemSectionGroup(path, title, itemSectionGroup);
			if (ItemSections.Count > 0)
			{
				foreach (ItemSectionDto itemSectionDto in ItemSections)
				{
					itemSectionDto.ItemSectionGroupId = itemSectionGroupDto.Id;
					itemSectionDto.AddItemSection(path, title, itemSectionDto);
				}
			}

		}

		public void UpdateItemSectionGroup()
		{

		}

		public void DeleteItemSectionGroup()
		{

		}

		public void CopyItemSectionGroup()
		{

		}

		public void AddMetaDataItem()
		{

		}

		public void AddNote()
		{

		}
		public List<ItemSectionGroupDto> LoadItemSectionGroups(string path, string title)
		{
			List<ItemSectionGroupDto> itemSectionGroupDtos = new List<ItemSectionGroupDto>();

			List<ItemSectionGroup> itemSectionGroups = new SupplementaryRepository().GetItemSectionGroups(path, title);
			if (itemSectionGroups.Count > 0)
			{
				foreach (ItemSectionGroup itemSectionGroup in itemSectionGroups)
				{
					ItemSectionGroupDto itemSectionGroupDto = new ItemSectionGroupDto()
					{
						Id = itemSectionGroup.Id,
						Title = itemSectionGroup.Title,
						Synopsis = itemSectionGroup.Synopsis,
						DateCreated = Convert.ToDateTime(itemSectionGroup.DateCreated).ToLocalTime(),
						IsNew = false
					};

					itemSectionGroupDto.ItemSections = new ItemSectionDto().LoadItemSectionsByGroup(path, title, itemSectionGroupDto.Id);

					itemSectionGroupDtos.Add(itemSectionGroupDto);
				}
			}

			return itemSectionGroupDtos;
		}


		private List<MetaDataItemDto> GetMetaDataItems()
		{
			return new List<MetaDataItemDto>();
		}
		private List<NoteDto> GetNotes()
		{
			return new List<NoteDto>();
		}
		private List<RevisionDto> GetRevisions()
		{
			return new List<RevisionDto>();
		}
	}
}
