﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Writer.Business;
using Writer.UI.Controls;
using System.IO;

namespace Writer.UI
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
		}

		private void mnuNewProject_Click(object sender, EventArgs e)
		{
			ProjectLayoutDto projectLayout = new ProjectLayoutDto();

			frmProjectWizard frmProjectWizard = new frmProjectWizard(projectLayout);
			frmProjectWizard.ShowDialog();

			Text = "Writer - " + projectLayout.Title;
		}

		private void mnuOpenProject_Click(object sender, EventArgs e)
		{
			if (ofDialog.ShowDialog() == DialogResult.OK)
			{
				ProjectLayoutDto projectLayout = new ProjectLayoutDto();
				projectLayout.DesktopPath = Path.GetDirectoryName(ofDialog.FileName);
				projectLayout.Title = Path.GetFileNameWithoutExtension(ofDialog.FileName);
				projectLayout.GetProject();

				Text = "Writer - " + projectLayout.Title;

				tvwLayout.Nodes.Clear();

				LoadDefaultLayout(projectLayout.Title);

				TreeNode treeNodeType = tvwLayout.Nodes[0];
				treeNodeType.Tag = projectLayout;

				if (projectLayout.Plots.Count > 0)
				{
					TreeNode treeNodePlots = tvwLayout.Nodes[3];

					int index = 1;
					foreach (PlotDto plotDto in projectLayout.Plots)
					{
						TreeNode treeNodePlot = new TreeNode()
						{
							Name = plotDto.Title,
							Text = plotDto.Title,
							Tag = plotDto
						};
						if (string.IsNullOrEmpty(plotDto.Title))
							treeNodePlot.Text = "Plot " + index.ToString();

						treeNodePlots.Nodes.Add(treeNodePlot);
						index++;
					}
				}

				if (projectLayout.Themes.Count > 0)
				{
					TreeNode treeNodeThemes = tvwLayout.Nodes[2];

					int index = 1;
					foreach (ThemeDto themeDto in projectLayout.Themes)
					{
						TreeNode treeNodeTheme = new TreeNode()
						{
							Name = themeDto.Title,
							Text = themeDto.Title,
							Tag = themeDto
						};
						if (string.IsNullOrEmpty(themeDto.Title))
							treeNodeTheme.Text = "Theme " + index.ToString();

						treeNodeThemes.Nodes.Add(treeNodeTheme);
						index++;
					}
				}

				if (projectLayout.Chapters.Count > 0)
				{

					TreeNode treeNodeDocument = tvwLayout.Nodes[1];

					TreeNode treeNodeChapters = new TreeNode()
					{
						Name = "Chapters",
						Text = "Chapters"
					};
					treeNodeDocument.Nodes.Add(treeNodeChapters);

					foreach (ChapterDto chapter in projectLayout.Chapters)
					{
						TreeNode treeNodeChapter = new TreeNode()
						{
							Name = "Chapter" + chapter.SortOrder.ToString(),
							Text = chapter.Title,
							Tag = chapter
						};
						if (string.IsNullOrEmpty(chapter.Title))
							treeNodeChapter.Text = "Chapter " + chapter.SortOrder.ToString();

						treeNodeChapters.Nodes.Add(treeNodeChapter);

						if (chapter.SceneCount > 0)
						{
							foreach (SceneDto scene in chapter.Scenes)
							{
								TreeNode treeNodeScene = new TreeNode()
								{
									Name = "Scene" + scene.SortOrder.ToString(),
									Text = scene.Title,
									Tag = scene
								};
								treeNodeChapter.Nodes.Add(treeNodeScene);
							}
						}
					}
				}

				if (projectLayout.Locations.Count > 0)
				{
					TreeNode treeNodeLocations = tvwLayout.Nodes[5];

					int index = 1;
					foreach (LocationDto locationDto in projectLayout.Locations)
					{
						TreeNode treeNodeLocation = new TreeNode()
						{
							Name = locationDto.Title,
							Text = locationDto.Title,
							Tag = locationDto
						};
						if (string.IsNullOrEmpty(locationDto.Title))
							treeNodeLocation.Text = "Location " + index.ToString();

						treeNodeLocations.Nodes.Add(treeNodeLocation);
						index++;
					}
				}

				if (projectLayout.Characters.Count > 0)
				{
					TreeNode treeNodeCharacters = tvwLayout.Nodes[4];

					int index = 1;
					foreach (CharacterDto characterDto in projectLayout.Characters)
					{
						TreeNode treeNodeCharacter = new TreeNode()
						{
							Name = characterDto.Title,
							Text = characterDto.Title,
							Tag = characterDto
						};
						if (string.IsNullOrEmpty(characterDto.Title))
							treeNodeCharacter.Text = "Character " + index.ToString();

						treeNodeCharacters.Nodes.Add(treeNodeCharacter);
						index++;
					}
				}

				if (projectLayout.StoryObjects.Count > 0)
				{
					TreeNode treeNodeObjects = tvwLayout.Nodes[7];

					int index = 1;
					foreach (StoryObjectDto storyObjectDto in projectLayout.StoryObjects)
					{
						TreeNode treeNodeObject = new TreeNode()
						{
							Name = storyObjectDto.Title,
							Text = storyObjectDto.Title,
							Tag = storyObjectDto
						};
						if (string.IsNullOrEmpty(storyObjectDto.Title))
							treeNodeObject.Text = "Object " + index.ToString();

						treeNodeObjects.Nodes.Add(treeNodeObject);
						index++;
					}
				}

				if (projectLayout.StoryEvents.Count > 0)
				{
					TreeNode treeNodeEvents = tvwLayout.Nodes[6];

					int index = 1;
					foreach (StoryEventDto storyEventDto in projectLayout.StoryEvents)
					{
						TreeNode treeNodeEvent = new TreeNode()
						{
							Name = storyEventDto.Title,
							Text = storyEventDto.Title,
							Tag = storyEventDto
						};
						if (string.IsNullOrEmpty(storyEventDto.Title))
							treeNodeEvent.Text = "Event " + index.ToString();

						treeNodeEvents.Nodes.Add(treeNodeEvent);
						index++;
					}
				}

				tvwLayout.ExpandAll();

			}
		}

		private void LoadDefaultLayout(string typeName)
		{
			TreeNode treeNodeType = new TreeNode()
			{
				Name = "DocumentType",
				Text = typeName
			};
			tvwLayout.Nodes.Add(treeNodeType);

			TreeNode treeNodePremise = new TreeNode()
			{
				Name = "Premise",
				Text = "Premise"
			};
			treeNodeType.Nodes.Add(treeNodePremise);

			TreeNode treeNodeDocument = new TreeNode()
			{
				Name = "DocumentLayout",
				Text = "Document"
			};
			tvwLayout.Nodes.Add(treeNodeDocument);

			TreeNode treeNodeTheme = new TreeNode()
			{
				Name = "Themes",
				Text = "Themes"
			};
			tvwLayout.Nodes.Add(treeNodeTheme);

			TreeNode treeNodePlot = new TreeNode()
			{
				Name = "Plots",
				Text = "Plots"
			};
			tvwLayout.Nodes.Add(treeNodePlot);

			TreeNode treeNodeCharacters = new TreeNode()
			{
				Name = "Characters",
				Text = "Characters"
			};
			tvwLayout.Nodes.Add(treeNodeCharacters);

			TreeNode treeNodeLocations = new TreeNode()
			{
				Name = "Locations",
				Text = "Locations"
			};
			tvwLayout.Nodes.Add(treeNodeLocations);

			TreeNode treeNodeEvents = new TreeNode()
			{
				Name = "Events",
				Text = "Events"
			};
			tvwLayout.Nodes.Add(treeNodeEvents);

			TreeNode treeNodeObjects = new TreeNode()
			{
				Name = "Objects",
				Text = "Objects"
			};
			tvwLayout.Nodes.Add(treeNodeObjects);

			TreeNode treeNodeResearch = new TreeNode()
			{
				Name = "Research",
				Text = "Research"
			};
			tvwLayout.Nodes.Add(treeNodeResearch);

			tvwLayout.ExpandAll();
		}

		private void frmMain_Load(object sender, EventArgs e)
		{
			LoadDefaultLayout("Default Doucument");
		}

		private void tvwLayout_AfterSelect(object sender, TreeViewEventArgs e)
		{

			if (e.Node.Tag is ProjectLayoutDto)
				rtbMain.Rtf = (e.Node.Tag as ProjectLayoutDto).Synopsis;
			else if (e.Node.Text == "Premise")
				rtbMain.Rtf = (e.Node.Parent.Tag as ProjectLayoutDto).Premise;
			else if (e.Node.Tag is ProjectLayoutDto)
				rtbMain.Rtf = (e.Node.Tag as ProjectLayoutDto).Synopsis;
			else if (e.Node.Tag is ChapterDto)
				rtbMain.Rtf = (e.Node.Tag as ChapterDto).Synopsis;
			else if (e.Node.Tag is SceneDto)
			{
				rtbMain.Rtf = (e.Node.Tag as SceneDto).SceneContent;
			}
		}

		private void tvwLayout_BeforeSelect(object sender, TreeViewCancelEventArgs e)
		{
			if (tvwLayout.SelectedNode?.Tag is SceneDto)
				(tvwLayout.SelectedNode.Tag as SceneDto).SceneContent = rtbMain.Rtf;
		}

		private void btnBold_Click(object sender, EventArgs e)
		{
			int selstart = rtbMain.SelectionStart;
			int sellength = rtbMain.SelectionLength;

			// Set font of selected text
			// You can use FontStyle.Bold | FontStyle.Italic to apply more than one style
			rtbMain.SelectionFont = new Font(rtbMain.Font, FontStyle.Bold);

			// Set cursor after selected text
			rtbMain.SelectionStart = rtbMain.SelectionStart + rtbMain.SelectionLength;
			rtbMain.SelectionLength = 0;
			// Set font immediately after selection
			rtbMain.SelectionFont = rtbMain.Font;

			// Reselect previous text
			rtbMain.Select(selstart, sellength);
		}
	}
}
