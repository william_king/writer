﻿
namespace Writer.UI
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewProject = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuOpenProject = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuSaveProject = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSaveProjectAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuCloaseAllProjects = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuCloseProject = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.ofDialog = new System.Windows.Forms.OpenFileDialog();
			this.scMain = new System.Windows.Forms.SplitContainer();
			this.pnlStructureHeader = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlMain = new System.Windows.Forms.Panel();
			this.tctCollections = new System.Windows.Forms.TabControl();
			this.tpLayout = new System.Windows.Forms.TabPage();
			this.pnlLayoutBottom = new System.Windows.Forms.Panel();
			this.tvwLayout = new System.Windows.Forms.TreeView();
			this.toolStrip2 = new System.Windows.Forms.ToolStrip();
			this.panel2 = new System.Windows.Forms.Panel();
			this.rtbMain = new System.Windows.Forms.RichTextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.scSecond = new System.Windows.Forms.SplitContainer();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.btnBold = new System.Windows.Forms.ToolStripButton();
			this.btnItalic = new System.Windows.Forms.ToolStripButton();
			this.btnUnderline = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnTextColor = new System.Windows.Forms.ToolStripButton();
			this.btnBackColor = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.cboFonts = new System.Windows.Forms.ToolStripComboBox();
			this.cboFontStyle = new System.Windows.Forms.ToolStripComboBox();
			this.cboSize = new System.Windows.Forms.ToolStripComboBox();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
			this.scMain.Panel1.SuspendLayout();
			this.scMain.Panel2.SuspendLayout();
			this.scMain.SuspendLayout();
			this.pnlStructureHeader.SuspendLayout();
			this.pnlMain.SuspendLayout();
			this.tctCollections.SuspendLayout();
			this.tpLayout.SuspendLayout();
			this.toolStrip2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.scSecond)).BeginInit();
			this.scSecond.Panel1.SuspendLayout();
			this.scSecond.Panel2.SuspendLayout();
			this.scSecond.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1004, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewProject,
            this.mnuOpenProject,
            this.toolStripMenuItem1,
            this.mnuSaveProject,
            this.mnuSaveProjectAs,
            this.toolStripSeparator1,
            this.mnuCloaseAllProjects,
            this.mnuCloseProject,
            this.toolStripMenuItem2,
            this.mnuExit});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// mnuNewProject
			// 
			this.mnuNewProject.Name = "mnuNewProject";
			this.mnuNewProject.Size = new System.Drawing.Size(180, 22);
			this.mnuNewProject.Text = "New Project";
			this.mnuNewProject.Click += new System.EventHandler(this.mnuNewProject_Click);
			// 
			// mnuOpenProject
			// 
			this.mnuOpenProject.Name = "mnuOpenProject";
			this.mnuOpenProject.Size = new System.Drawing.Size(180, 22);
			this.mnuOpenProject.Text = "Open Project";
			this.mnuOpenProject.Click += new System.EventHandler(this.mnuOpenProject_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
			// 
			// mnuSaveProject
			// 
			this.mnuSaveProject.Name = "mnuSaveProject";
			this.mnuSaveProject.Size = new System.Drawing.Size(180, 22);
			this.mnuSaveProject.Text = "Save Project";
			// 
			// mnuSaveProjectAs
			// 
			this.mnuSaveProjectAs.Name = "mnuSaveProjectAs";
			this.mnuSaveProjectAs.Size = new System.Drawing.Size(180, 22);
			this.mnuSaveProjectAs.Text = "Save Project As";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
			// 
			// mnuCloaseAllProjects
			// 
			this.mnuCloaseAllProjects.Name = "mnuCloaseAllProjects";
			this.mnuCloaseAllProjects.Size = new System.Drawing.Size(180, 22);
			this.mnuCloaseAllProjects.Text = "Close All Projects";
			// 
			// mnuCloseProject
			// 
			this.mnuCloseProject.Name = "mnuCloseProject";
			this.mnuCloseProject.Size = new System.Drawing.Size(180, 22);
			this.mnuCloseProject.Text = "Close Project";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
			// 
			// mnuExit
			// 
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Size = new System.Drawing.Size(180, 22);
			this.mnuExit.Text = "Exit";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Location = new System.Drawing.Point(0, 24);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(1004, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// ofDialog
			// 
			this.ofDialog.Filter = "Writer files (*.wrp)|*.wrp";
			this.ofDialog.RestoreDirectory = true;
			// 
			// scMain
			// 
			this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scMain.Location = new System.Drawing.Point(0, 49);
			this.scMain.Name = "scMain";
			// 
			// scMain.Panel1
			// 
			this.scMain.Panel1.Controls.Add(this.pnlMain);
			this.scMain.Panel1.Controls.Add(this.pnlStructureHeader);
			// 
			// scMain.Panel2
			// 
			this.scMain.Panel2.Controls.Add(this.scSecond);
			this.scMain.Panel2.Controls.Add(this.toolStrip2);
			this.scMain.Size = new System.Drawing.Size(1004, 498);
			this.scMain.SplitterDistance = 150;
			this.scMain.TabIndex = 2;
			// 
			// pnlStructureHeader
			// 
			this.pnlStructureHeader.Controls.Add(this.label1);
			this.pnlStructureHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlStructureHeader.Location = new System.Drawing.Point(0, 0);
			this.pnlStructureHeader.Name = "pnlStructureHeader";
			this.pnlStructureHeader.Size = new System.Drawing.Size(148, 30);
			this.pnlStructureHeader.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.SystemColors.ControlDark;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(148, 30);
			this.label1.TabIndex = 2;
			this.label1.Text = "Collections";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.tctCollections);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(0, 30);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(148, 466);
			this.pnlMain.TabIndex = 3;
			// 
			// tctCollections
			// 
			this.tctCollections.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
			this.tctCollections.Controls.Add(this.tpLayout);
			this.tctCollections.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tctCollections.Location = new System.Drawing.Point(0, 0);
			this.tctCollections.Multiline = true;
			this.tctCollections.Name = "tctCollections";
			this.tctCollections.SelectedIndex = 0;
			this.tctCollections.Size = new System.Drawing.Size(148, 466);
			this.tctCollections.TabIndex = 2;
			// 
			// tpLayout
			// 
			this.tpLayout.Controls.Add(this.tvwLayout);
			this.tpLayout.Controls.Add(this.pnlLayoutBottom);
			this.tpLayout.Location = new System.Drawing.Point(4, 25);
			this.tpLayout.Name = "tpLayout";
			this.tpLayout.Padding = new System.Windows.Forms.Padding(3);
			this.tpLayout.Size = new System.Drawing.Size(140, 437);
			this.tpLayout.TabIndex = 0;
			this.tpLayout.Text = "Layout";
			this.tpLayout.UseVisualStyleBackColor = true;
			// 
			// pnlLayoutBottom
			// 
			this.pnlLayoutBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlLayoutBottom.Location = new System.Drawing.Point(3, 406);
			this.pnlLayoutBottom.Name = "pnlLayoutBottom";
			this.pnlLayoutBottom.Size = new System.Drawing.Size(134, 28);
			this.pnlLayoutBottom.TabIndex = 0;
			// 
			// tvwLayout
			// 
			this.tvwLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvwLayout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tvwLayout.Location = new System.Drawing.Point(3, 3);
			this.tvwLayout.Name = "tvwLayout";
			this.tvwLayout.ShowLines = false;
			this.tvwLayout.ShowRootLines = false;
			this.tvwLayout.Size = new System.Drawing.Size(134, 403);
			this.tvwLayout.TabIndex = 1;
			this.tvwLayout.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvwLayout_BeforeSelect);
			this.tvwLayout.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwLayout_AfterSelect);
			// 
			// toolStrip2
			// 
			this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBold,
            this.btnItalic,
            this.btnUnderline,
            this.toolStripSeparator2,
            this.btnTextColor,
            this.btnBackColor,
            this.toolStripSeparator3,
            this.cboFonts,
            this.cboFontStyle,
            this.cboSize});
			this.toolStrip2.Location = new System.Drawing.Point(0, 0);
			this.toolStrip2.Name = "toolStrip2";
			this.toolStrip2.Size = new System.Drawing.Size(850, 26);
			this.toolStrip2.TabIndex = 0;
			this.toolStrip2.Text = "toolStrip2";
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 442);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(194, 28);
			this.panel2.TabIndex = 1;
			// 
			// rtbMain
			// 
			this.rtbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtbMain.BackColor = System.Drawing.SystemColors.Control;
			this.rtbMain.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.rtbMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtbMain.Location = new System.Drawing.Point(77, 30);
			this.rtbMain.Name = "rtbMain";
			this.rtbMain.Size = new System.Drawing.Size(510, 411);
			this.rtbMain.TabIndex = 0;
			this.rtbMain.Text = "";
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 442);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(648, 28);
			this.panel1.TabIndex = 1;
			// 
			// scSecond
			// 
			this.scSecond.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.scSecond.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scSecond.Location = new System.Drawing.Point(0, 26);
			this.scSecond.Name = "scSecond";
			// 
			// scSecond.Panel1
			// 
			this.scSecond.Panel1.BackColor = System.Drawing.SystemColors.ControlLight;
			this.scSecond.Panel1.Controls.Add(this.panel3);
			this.scSecond.Panel1.Controls.Add(this.panel1);
			this.scSecond.Panel1.Controls.Add(this.rtbMain);
			// 
			// scSecond.Panel2
			// 
			this.scSecond.Panel2.Controls.Add(this.panel4);
			this.scSecond.Panel2.Controls.Add(this.panel2);
			this.scSecond.Size = new System.Drawing.Size(850, 472);
			this.scSecond.SplitterDistance = 650;
			this.scSecond.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.SystemColors.Control;
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(648, 31);
			this.panel3.TabIndex = 3;
			// 
			// panel4
			// 
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(194, 31);
			this.panel4.TabIndex = 4;
			// 
			// btnBold
			// 
			this.btnBold.CheckOnClick = true;
			this.btnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnBold.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
			this.btnBold.Image = ((System.Drawing.Image)(resources.GetObject("btnBold.Image")));
			this.btnBold.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBold.Name = "btnBold";
			this.btnBold.Size = new System.Drawing.Size(23, 23);
			this.btnBold.Text = "B";
			this.btnBold.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
			// 
			// btnItalic
			// 
			this.btnItalic.CheckOnClick = true;
			this.btnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnItalic.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic);
			this.btnItalic.Image = ((System.Drawing.Image)(resources.GetObject("btnItalic.Image")));
			this.btnItalic.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnItalic.Name = "btnItalic";
			this.btnItalic.Size = new System.Drawing.Size(23, 23);
			this.btnItalic.Text = "I";
			// 
			// btnUnderline
			// 
			this.btnUnderline.CheckOnClick = true;
			this.btnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnUnderline.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Underline);
			this.btnUnderline.Image = ((System.Drawing.Image)(resources.GetObject("btnUnderline.Image")));
			this.btnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnUnderline.Name = "btnUnderline";
			this.btnUnderline.Size = new System.Drawing.Size(23, 23);
			this.btnUnderline.Text = "U";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 26);
			// 
			// btnTextColor
			// 
			this.btnTextColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnTextColor.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
			this.btnTextColor.Image = ((System.Drawing.Image)(resources.GetObject("btnTextColor.Image")));
			this.btnTextColor.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnTextColor.Name = "btnTextColor";
			this.btnTextColor.Size = new System.Drawing.Size(23, 23);
			this.btnTextColor.Text = "A";
			// 
			// btnBackColor
			// 
			this.btnBackColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnBackColor.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColor.Image")));
			this.btnBackColor.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColor.Name = "btnBackColor";
			this.btnBackColor.Size = new System.Drawing.Size(23, 23);
			this.btnBackColor.Text = "C";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 26);
			// 
			// cboFonts
			// 
			this.cboFonts.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cboFonts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFonts.Name = "cboFonts";
			this.cboFonts.Size = new System.Drawing.Size(121, 26);
			// 
			// cboFontStyle
			// 
			this.cboFontStyle.AutoToolTip = true;
			this.cboFontStyle.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cboFontStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFontStyle.Items.AddRange(new object[] {
            "Regular",
            "Bold",
            "Bold Italic",
            "Italic"});
			this.cboFontStyle.Name = "cboFontStyle";
			this.cboFontStyle.Size = new System.Drawing.Size(121, 26);
			// 
			// cboSize
			// 
			this.cboSize.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cboSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSize.Items.AddRange(new object[] {
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "18",
            "24",
            "36",
            "48",
            "64",
            "72",
            "96",
            "144",
            "188"});
			this.cboSize.Name = "cboSize";
			this.cboSize.Size = new System.Drawing.Size(121, 26);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1004, 547);
			this.Controls.Add(this.scMain);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "frmMain";
			this.Text = "Form1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.scMain.Panel1.ResumeLayout(false);
			this.scMain.Panel2.ResumeLayout(false);
			this.scMain.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
			this.scMain.ResumeLayout(false);
			this.pnlStructureHeader.ResumeLayout(false);
			this.pnlMain.ResumeLayout(false);
			this.tctCollections.ResumeLayout(false);
			this.tpLayout.ResumeLayout(false);
			this.toolStrip2.ResumeLayout(false);
			this.toolStrip2.PerformLayout();
			this.scSecond.Panel1.ResumeLayout(false);
			this.scSecond.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.scSecond)).EndInit();
			this.scSecond.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem mnuNewProject;
		private System.Windows.Forms.ToolStripMenuItem mnuOpenProject;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mnuSaveProject;
		private System.Windows.Forms.ToolStripMenuItem mnuSaveProjectAs;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem mnuCloaseAllProjects;
		private System.Windows.Forms.ToolStripMenuItem mnuCloseProject;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem mnuExit;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.OpenFileDialog ofDialog;
		private System.Windows.Forms.SplitContainer scMain;
		private System.Windows.Forms.Panel pnlStructureHeader;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel pnlMain;
		private System.Windows.Forms.TabControl tctCollections;
		private System.Windows.Forms.TabPage tpLayout;
		private System.Windows.Forms.TreeView tvwLayout;
		private System.Windows.Forms.Panel pnlLayoutBottom;
		private System.Windows.Forms.SplitContainer scSecond;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RichTextBox rtbMain;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ToolStrip toolStrip2;
		private System.Windows.Forms.ToolStripButton btnBold;
		private System.Windows.Forms.ToolStripButton btnItalic;
		private System.Windows.Forms.ToolStripButton btnUnderline;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton btnTextColor;
		private System.Windows.Forms.ToolStripButton btnBackColor;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripComboBox cboFonts;
		private System.Windows.Forms.ToolStripComboBox cboFontStyle;
		private System.Windows.Forms.ToolStripComboBox cboSize;
	}
}

