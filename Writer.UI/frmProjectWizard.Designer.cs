﻿
namespace Writer.UI
{
	partial class frmProjectWizard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.btnPrevious = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.pnlStep1 = new System.Windows.Forms.Panel();
			this.lstTemplates = new System.Windows.Forms.ListBox();
			this.lstTemplateTypes = new System.Windows.Forms.ListBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlStep2 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnFolder = new System.Windows.Forms.Button();
			this.txtSynopsis = new System.Windows.Forms.TextBox();
			this.txtAuthor = new System.Windows.Forms.TextBox();
			this.txtProjectLocation = new System.Windows.Forms.TextBox();
			this.txtProjectName = new System.Windows.Forms.TextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.pnlStep3 = new System.Windows.Forms.Panel();
			this.label12 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.dtpFinalEditEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpFinalEditStart = new System.Windows.Forms.DateTimePicker();
			this.dtpEdit2End = new System.Windows.Forms.DateTimePicker();
			this.dtpEdit2Start = new System.Windows.Forms.DateTimePicker();
			this.dtpEdit1End = new System.Windows.Forms.DateTimePicker();
			this.dtpEdit1Start = new System.Windows.Forms.DateTimePicker();
			this.dtpDraftEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpDraftStart = new System.Windows.Forms.DateTimePicker();
			this.dtpOutlineEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpOutlineStart = new System.Windows.Forms.DateTimePicker();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label11 = new System.Windows.Forms.Label();
			this.pnlStep4 = new System.Windows.Forms.Panel();
			this.txtCustomField4 = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txtCustomField3 = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtCustomField2 = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.txtCustomField1 = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.panel5 = new System.Windows.Forms.Panel();
			this.label18 = new System.Windows.Forms.Label();
			this.fbDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.pnlBottom.SuspendLayout();
			this.pnlStep1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.pnlStep2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.pnlStep3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.pnlStep4.SuspendLayout();
			this.panel5.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlBottom
			// 
			this.pnlBottom.Controls.Add(this.btnPrevious);
			this.pnlBottom.Controls.Add(this.btnCancel);
			this.pnlBottom.Controls.Add(this.btnNext);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottom.Location = new System.Drawing.Point(0, 398);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new System.Drawing.Size(800, 52);
			this.pnlBottom.TabIndex = 0;
			// 
			// btnPrevious
			// 
			this.btnPrevious.Location = new System.Drawing.Point(12, 4);
			this.btnPrevious.Name = "btnPrevious";
			this.btnPrevious.Size = new System.Drawing.Size(75, 43);
			this.btnPrevious.TabIndex = 2;
			this.btnPrevious.Text = "Back";
			this.btnPrevious.UseVisualStyleBackColor = true;
			this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(632, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 43);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnNext
			// 
			this.btnNext.Location = new System.Drawing.Point(713, 4);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(75, 43);
			this.btnNext.TabIndex = 0;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// pnlStep1
			// 
			this.pnlStep1.Controls.Add(this.lstTemplates);
			this.pnlStep1.Controls.Add(this.lstTemplateTypes);
			this.pnlStep1.Controls.Add(this.panel1);
			this.pnlStep1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlStep1.Location = new System.Drawing.Point(0, 0);
			this.pnlStep1.Name = "pnlStep1";
			this.pnlStep1.Size = new System.Drawing.Size(800, 450);
			this.pnlStep1.TabIndex = 1;
			// 
			// lstTemplates
			// 
			this.lstTemplates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lstTemplates.Dock = System.Windows.Forms.DockStyle.Left;
			this.lstTemplates.FormattingEnabled = true;
			this.lstTemplates.Location = new System.Drawing.Point(191, 40);
			this.lstTemplates.Name = "lstTemplates";
			this.lstTemplates.Size = new System.Drawing.Size(496, 410);
			this.lstTemplates.TabIndex = 2;
			// 
			// lstTemplateTypes
			// 
			this.lstTemplateTypes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lstTemplateTypes.Dock = System.Windows.Forms.DockStyle.Left;
			this.lstTemplateTypes.FormattingEnabled = true;
			this.lstTemplateTypes.Items.AddRange(new object[] {
            "Short Story",
            "Fiction",
            "Non-Fiction"});
			this.lstTemplateTypes.Location = new System.Drawing.Point(0, 40);
			this.lstTemplateTypes.Name = "lstTemplateTypes";
			this.lstTemplateTypes.Size = new System.Drawing.Size(191, 410);
			this.lstTemplateTypes.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(800, 40);
			this.panel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "label1";
			// 
			// pnlStep2
			// 
			this.pnlStep2.Controls.Add(this.label6);
			this.pnlStep2.Controls.Add(this.label5);
			this.pnlStep2.Controls.Add(this.label4);
			this.pnlStep2.Controls.Add(this.label3);
			this.pnlStep2.Controls.Add(this.btnFolder);
			this.pnlStep2.Controls.Add(this.txtSynopsis);
			this.pnlStep2.Controls.Add(this.txtAuthor);
			this.pnlStep2.Controls.Add(this.txtProjectLocation);
			this.pnlStep2.Controls.Add(this.txtProjectName);
			this.pnlStep2.Controls.Add(this.panel3);
			this.pnlStep2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlStep2.Location = new System.Drawing.Point(0, 0);
			this.pnlStep2.Name = "pnlStep2";
			this.pnlStep2.Size = new System.Drawing.Size(800, 450);
			this.pnlStep2.TabIndex = 2;
			this.pnlStep2.Visible = false;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(21, 124);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(60, 13);
			this.label6.TabIndex = 7;
			this.label6.Text = "Description";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(21, 98);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(38, 13);
			this.label5.TabIndex = 8;
			this.label5.Text = "Author";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(21, 71);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(84, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Project Location";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(21, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(71, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Project Name";
			// 
			// btnFolder
			// 
			this.btnFolder.Location = new System.Drawing.Point(659, 71);
			this.btnFolder.Name = "btnFolder";
			this.btnFolder.Size = new System.Drawing.Size(37, 23);
			this.btnFolder.TabIndex = 5;
			this.btnFolder.Text = "...";
			this.btnFolder.UseVisualStyleBackColor = true;
			this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
			// 
			// txtSynopsis
			// 
			this.txtSynopsis.Location = new System.Drawing.Point(156, 124);
			this.txtSynopsis.Multiline = true;
			this.txtSynopsis.Name = "txtSynopsis";
			this.txtSynopsis.Size = new System.Drawing.Size(597, 257);
			this.txtSynopsis.TabIndex = 4;
			// 
			// txtAuthor
			// 
			this.txtAuthor.Location = new System.Drawing.Point(156, 98);
			this.txtAuthor.Name = "txtAuthor";
			this.txtAuthor.Size = new System.Drawing.Size(497, 20);
			this.txtAuthor.TabIndex = 3;
			// 
			// txtProjectLocation
			// 
			this.txtProjectLocation.Location = new System.Drawing.Point(156, 72);
			this.txtProjectLocation.Name = "txtProjectLocation";
			this.txtProjectLocation.Size = new System.Drawing.Size(497, 20);
			this.txtProjectLocation.TabIndex = 2;
			// 
			// txtProjectName
			// 
			this.txtProjectName.Location = new System.Drawing.Point(156, 46);
			this.txtProjectName.Name = "txtProjectName";
			this.txtProjectName.Size = new System.Drawing.Size(497, 20);
			this.txtProjectName.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label2);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(800, 40);
			this.panel3.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(800, 40);
			this.label2.TabIndex = 0;
			this.label2.Text = "label2";
			// 
			// pnlStep3
			// 
			this.pnlStep3.Controls.Add(this.label12);
			this.pnlStep3.Controls.Add(this.label9);
			this.pnlStep3.Controls.Add(this.dtpFinalEditEnd);
			this.pnlStep3.Controls.Add(this.dtpFinalEditStart);
			this.pnlStep3.Controls.Add(this.dtpEdit2End);
			this.pnlStep3.Controls.Add(this.dtpEdit2Start);
			this.pnlStep3.Controls.Add(this.dtpEdit1End);
			this.pnlStep3.Controls.Add(this.dtpEdit1Start);
			this.pnlStep3.Controls.Add(this.dtpDraftEnd);
			this.pnlStep3.Controls.Add(this.dtpDraftStart);
			this.pnlStep3.Controls.Add(this.dtpOutlineEnd);
			this.pnlStep3.Controls.Add(this.dtpOutlineStart);
			this.pnlStep3.Controls.Add(this.label7);
			this.pnlStep3.Controls.Add(this.label8);
			this.pnlStep3.Controls.Add(this.label10);
			this.pnlStep3.Controls.Add(this.panel4);
			this.pnlStep3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlStep3.Location = new System.Drawing.Point(0, 0);
			this.pnlStep3.Name = "pnlStep3";
			this.pnlStep3.Size = new System.Drawing.Size(800, 450);
			this.pnlStep3.TabIndex = 3;
			this.pnlStep3.Visible = false;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(153, 215);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(50, 13);
			this.label12.TabIndex = 20;
			this.label12.Text = "Final Edit";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(153, 189);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(46, 13);
			this.label9.TabIndex = 19;
			this.label9.Text = "2nd Edit";
			// 
			// dtpFinalEditEnd
			// 
			this.dtpFinalEditEnd.CustomFormat = "yyyy-MM-dd";
			this.dtpFinalEditEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFinalEditEnd.Location = new System.Drawing.Point(406, 215);
			this.dtpFinalEditEnd.Name = "dtpFinalEditEnd";
			this.dtpFinalEditEnd.Size = new System.Drawing.Size(121, 20);
			this.dtpFinalEditEnd.TabIndex = 18;
			// 
			// dtpFinalEditStart
			// 
			this.dtpFinalEditStart.CustomFormat = "yyyy-MM-dd";
			this.dtpFinalEditStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFinalEditStart.Location = new System.Drawing.Point(241, 215);
			this.dtpFinalEditStart.Name = "dtpFinalEditStart";
			this.dtpFinalEditStart.Size = new System.Drawing.Size(121, 20);
			this.dtpFinalEditStart.TabIndex = 17;
			// 
			// dtpEdit2End
			// 
			this.dtpEdit2End.CustomFormat = "yyyy-MM-dd";
			this.dtpEdit2End.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpEdit2End.Location = new System.Drawing.Point(406, 189);
			this.dtpEdit2End.Name = "dtpEdit2End";
			this.dtpEdit2End.Size = new System.Drawing.Size(121, 20);
			this.dtpEdit2End.TabIndex = 16;
			// 
			// dtpEdit2Start
			// 
			this.dtpEdit2Start.CustomFormat = "yyyy-MM-dd";
			this.dtpEdit2Start.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpEdit2Start.Location = new System.Drawing.Point(241, 189);
			this.dtpEdit2Start.Name = "dtpEdit2Start";
			this.dtpEdit2Start.Size = new System.Drawing.Size(121, 20);
			this.dtpEdit2Start.TabIndex = 15;
			// 
			// dtpEdit1End
			// 
			this.dtpEdit1End.CustomFormat = "yyyy-MM-dd";
			this.dtpEdit1End.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpEdit1End.Location = new System.Drawing.Point(406, 163);
			this.dtpEdit1End.Name = "dtpEdit1End";
			this.dtpEdit1End.Size = new System.Drawing.Size(121, 20);
			this.dtpEdit1End.TabIndex = 14;
			// 
			// dtpEdit1Start
			// 
			this.dtpEdit1Start.CustomFormat = "yyyy-MM-dd";
			this.dtpEdit1Start.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpEdit1Start.Location = new System.Drawing.Point(241, 163);
			this.dtpEdit1Start.Name = "dtpEdit1Start";
			this.dtpEdit1Start.Size = new System.Drawing.Size(121, 20);
			this.dtpEdit1Start.TabIndex = 13;
			// 
			// dtpDraftEnd
			// 
			this.dtpDraftEnd.CustomFormat = "yyyy-MM-dd";
			this.dtpDraftEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDraftEnd.Location = new System.Drawing.Point(406, 137);
			this.dtpDraftEnd.Name = "dtpDraftEnd";
			this.dtpDraftEnd.Size = new System.Drawing.Size(121, 20);
			this.dtpDraftEnd.TabIndex = 12;
			// 
			// dtpDraftStart
			// 
			this.dtpDraftStart.CustomFormat = "yyyy-MM-dd";
			this.dtpDraftStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDraftStart.Location = new System.Drawing.Point(241, 137);
			this.dtpDraftStart.Name = "dtpDraftStart";
			this.dtpDraftStart.Size = new System.Drawing.Size(121, 20);
			this.dtpDraftStart.TabIndex = 11;
			// 
			// dtpOutlineEnd
			// 
			this.dtpOutlineEnd.CustomFormat = "yyyy-MM-dd";
			this.dtpOutlineEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpOutlineEnd.Location = new System.Drawing.Point(406, 111);
			this.dtpOutlineEnd.Name = "dtpOutlineEnd";
			this.dtpOutlineEnd.Size = new System.Drawing.Size(121, 20);
			this.dtpOutlineEnd.TabIndex = 10;
			// 
			// dtpOutlineStart
			// 
			this.dtpOutlineStart.CustomFormat = "yyyy-MM-dd";
			this.dtpOutlineStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpOutlineStart.Location = new System.Drawing.Point(241, 111);
			this.dtpOutlineStart.Name = "dtpOutlineStart";
			this.dtpOutlineStart.Size = new System.Drawing.Size(121, 20);
			this.dtpOutlineStart.TabIndex = 9;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(153, 163);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(42, 13);
			this.label7.TabIndex = 7;
			this.label7.Text = "1st Edit";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(153, 137);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(30, 13);
			this.label8.TabIndex = 8;
			this.label8.Text = "Draft";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(153, 113);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(40, 13);
			this.label10.TabIndex = 6;
			this.label10.Text = "Outline";
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.label11);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(800, 40);
			this.panel4.TabIndex = 0;
			// 
			// label11
			// 
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Location = new System.Drawing.Point(0, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(800, 40);
			this.label11.TabIndex = 0;
			this.label11.Text = "Targets";
			// 
			// pnlStep4
			// 
			this.pnlStep4.Controls.Add(this.txtCustomField4);
			this.pnlStep4.Controls.Add(this.label16);
			this.pnlStep4.Controls.Add(this.txtCustomField3);
			this.pnlStep4.Controls.Add(this.label15);
			this.pnlStep4.Controls.Add(this.txtCustomField2);
			this.pnlStep4.Controls.Add(this.label14);
			this.pnlStep4.Controls.Add(this.txtCustomField1);
			this.pnlStep4.Controls.Add(this.label13);
			this.pnlStep4.Controls.Add(this.panel5);
			this.pnlStep4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlStep4.Location = new System.Drawing.Point(0, 0);
			this.pnlStep4.Name = "pnlStep4";
			this.pnlStep4.Size = new System.Drawing.Size(800, 450);
			this.pnlStep4.TabIndex = 4;
			this.pnlStep4.Visible = false;
			// 
			// txtCustomField4
			// 
			this.txtCustomField4.Location = new System.Drawing.Point(83, 127);
			this.txtCustomField4.Name = "txtCustomField4";
			this.txtCustomField4.Size = new System.Drawing.Size(198, 20);
			this.txtCustomField4.TabIndex = 8;
			this.txtCustomField4.Text = "Quality";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(21, 130);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(38, 13);
			this.label16.TabIndex = 7;
			this.label16.Text = "Field 4";
			// 
			// txtCustomField3
			// 
			this.txtCustomField3.Location = new System.Drawing.Point(83, 101);
			this.txtCustomField3.Name = "txtCustomField3";
			this.txtCustomField3.Size = new System.Drawing.Size(198, 20);
			this.txtCustomField3.TabIndex = 6;
			this.txtCustomField3.Text = "Humour";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(21, 104);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(38, 13);
			this.label15.TabIndex = 5;
			this.label15.Text = "Field 3";
			// 
			// txtCustomField2
			// 
			this.txtCustomField2.Location = new System.Drawing.Point(83, 75);
			this.txtCustomField2.Name = "txtCustomField2";
			this.txtCustomField2.Size = new System.Drawing.Size(198, 20);
			this.txtCustomField2.TabIndex = 4;
			this.txtCustomField2.Text = "Tension";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(21, 78);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(38, 13);
			this.label14.TabIndex = 3;
			this.label14.Text = "Field 2";
			// 
			// txtCustomField1
			// 
			this.txtCustomField1.Location = new System.Drawing.Point(83, 49);
			this.txtCustomField1.Name = "txtCustomField1";
			this.txtCustomField1.Size = new System.Drawing.Size(198, 20);
			this.txtCustomField1.TabIndex = 2;
			this.txtCustomField1.Text = "Relevance";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(21, 52);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(38, 13);
			this.label13.TabIndex = 1;
			this.label13.Text = "Field 1";
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.label18);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel5.Location = new System.Drawing.Point(0, 0);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(800, 40);
			this.panel5.TabIndex = 0;
			// 
			// label18
			// 
			this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label18.Location = new System.Drawing.Point(0, 0);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(800, 40);
			this.label18.TabIndex = 0;
			this.label18.Text = "Custome Fields";
			// 
			// frmProjectWizard
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.pnlBottom);
			this.Controls.Add(this.pnlStep1);
			this.Controls.Add(this.pnlStep4);
			this.Controls.Add(this.pnlStep3);
			this.Controls.Add(this.pnlStep2);
			this.Name = "frmProjectWizard";
			this.Text = "Project Wizard";
			this.Load += new System.EventHandler(this.frmProjectWizard_Load);
			this.pnlBottom.ResumeLayout(false);
			this.pnlStep1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.pnlStep2.ResumeLayout(false);
			this.pnlStep2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.pnlStep3.ResumeLayout(false);
			this.pnlStep3.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.pnlStep4.ResumeLayout(false);
			this.pnlStep4.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlBottom;
		private System.Windows.Forms.Panel pnlStep1;
		private System.Windows.Forms.ListBox lstTemplates;
		private System.Windows.Forms.ListBox lstTemplateTypes;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel pnlStep2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnFolder;
		private System.Windows.Forms.TextBox txtSynopsis;
		private System.Windows.Forms.TextBox txtAuthor;
		private System.Windows.Forms.TextBox txtProjectLocation;
		private System.Windows.Forms.TextBox txtProjectName;
		private System.Windows.Forms.Panel pnlStep3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button btnPrevious;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.DateTimePicker dtpFinalEditEnd;
		private System.Windows.Forms.DateTimePicker dtpFinalEditStart;
		private System.Windows.Forms.DateTimePicker dtpEdit2End;
		private System.Windows.Forms.DateTimePicker dtpEdit2Start;
		private System.Windows.Forms.DateTimePicker dtpEdit1End;
		private System.Windows.Forms.DateTimePicker dtpEdit1Start;
		private System.Windows.Forms.DateTimePicker dtpDraftEnd;
		private System.Windows.Forms.DateTimePicker dtpDraftStart;
		private System.Windows.Forms.DateTimePicker dtpOutlineEnd;
		private System.Windows.Forms.DateTimePicker dtpOutlineStart;
		private System.Windows.Forms.Panel pnlStep4;
		private System.Windows.Forms.TextBox txtCustomField4;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtCustomField3;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox txtCustomField2;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox txtCustomField1;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.FolderBrowserDialog fbDialog;
	}
}