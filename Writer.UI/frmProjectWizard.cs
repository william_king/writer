﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Writer.Business;

namespace Writer.UI
{
	public partial class frmProjectWizard : Form
	{
		private ProjectLayoutDto _projectLayoutDto;

		public frmProjectWizard(ProjectLayoutDto projectLayoutDto)
		{
			_projectLayoutDto = projectLayoutDto;
			InitializeComponent();
		}

		private void btnFolder_Click(object sender, EventArgs e)
		{
			if (fbDialog.ShowDialog() == DialogResult.OK)
			{
				txtProjectLocation.Text = fbDialog.SelectedPath;
			}
		}

		private void frmProjectWizard_Load(object sender, EventArgs e)
		{
			pnlStep1.Visible = true;
		}

		private void btnNext_Click(object sender, EventArgs e)
		{
			if (pnlStep1.Visible)
			{
				pnlStep2.Visible = true;
				pnlStep1.Visible = false;
				btnNext.Text = "Next";
			}
			else if (pnlStep2.Visible)
			{
				pnlStep3.Visible = true;
				pnlStep2.Visible = false;
				btnNext.Text = "Next";
			}
			else if (pnlStep3.Visible)
			{
				pnlStep4.Visible = true;
				pnlStep3.Visible = false;
				btnNext.Text = "Save";
			}
			else if (pnlStep4.Visible) //save project
			{
				CreateProject();

				Close();
			}
		}

		private void btnPrevious_Click(object sender, EventArgs e)
		{
			if (pnlStep4.Visible)
			{
				pnlStep3.Visible = true;
				pnlStep4.Visible = false;
			}
			else if (pnlStep3.Visible)
			{
				pnlStep2.Visible = true;
				pnlStep3.Visible = false;
			}
			else if (pnlStep2.Visible)
			{
				pnlStep1.Visible = true;
				pnlStep2.Visible = false;
			}
		}

		private void CreateProject()
		{
			_projectLayoutDto.Title = txtProjectName.Text;
			_projectLayoutDto.Synopsis = txtSynopsis.Text;
			_projectLayoutDto.Author = txtAuthor.Text;
			_projectLayoutDto.DesktopPath = txtProjectLocation.Text;
			_projectLayoutDto.Version = 1;
			_projectLayoutDto.Premise = string.Empty;
			_projectLayoutDto.DateCreated = DateTime.Now;
			_projectLayoutDto.PlannedCompleteDate = DateTime.Now;
			_projectLayoutDto.CompleteDate = DateTime.Now;
			_projectLayoutDto.LastFullBackup = DateTime.Now;
			_projectLayoutDto.WordTarget = 0;
			_projectLayoutDto.IsCompleted = false;
			_projectLayoutDto.WordTargetStartDate = DateTime.Now;
			_projectLayoutDto.WordTargetEndDate = DateTime.Now;
			_projectLayoutDto.WordCountStart = 0;
			_projectLayoutDto.LastViewedSceneId = 0;

			var customField = new CustomFieldDto()
			{
				Title = txtCustomField1.Text,
				Synopsis = string.Empty, 
				DateCreated = DateTime.Now
			};
			_projectLayoutDto.CustomFields.Add(customField);

			customField = new CustomFieldDto()
			{
				Title = txtCustomField2.Text,
				Synopsis = string.Empty,
				DateCreated = DateTime.Now
			};
			_projectLayoutDto.CustomFields.Add(customField);

			customField = new CustomFieldDto()
			{
				Title = txtCustomField3.Text,
				Synopsis = string.Empty,
				DateCreated = DateTime.Now
			};
			_projectLayoutDto.CustomFields.Add(customField);

			customField = new CustomFieldDto()
			{
				Title = txtCustomField4.Text,
				Synopsis = string.Empty,
				DateCreated = DateTime.Now
			};
			_projectLayoutDto.CustomFields.Add(customField);

			TargetDto targetDto = new TargetDto()
			{
				DateCreated = DateTime.Now
			};
			_projectLayoutDto.Targets.Add(targetDto);

			_projectLayoutDto.CreateProject();
		}
	}
}
