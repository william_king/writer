﻿namespace Writer.UI.Controls
{
    partial class writerMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.scMain = new System.Windows.Forms.SplitContainer();
			this.tctCollections = new System.Windows.Forms.TabControl();
			this.tpLayout = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.pnlCollectionHeader = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.scSecond = new System.Windows.Forms.SplitContainer();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.rtbMainContent = new System.Windows.Forms.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
			this.scMain.Panel1.SuspendLayout();
			this.scMain.Panel2.SuspendLayout();
			this.scMain.SuspendLayout();
			this.tctCollections.SuspendLayout();
			this.pnlCollectionHeader.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.scSecond)).BeginInit();
			this.scSecond.Panel1.SuspendLayout();
			this.scSecond.SuspendLayout();
			this.SuspendLayout();
			// 
			// scMain
			// 
			this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scMain.Location = new System.Drawing.Point(0, 0);
			this.scMain.Name = "scMain";
			// 
			// scMain.Panel1
			// 
			this.scMain.Panel1.Controls.Add(this.tctCollections);
			this.scMain.Panel1.Controls.Add(this.pnlCollectionHeader);
			// 
			// scMain.Panel2
			// 
			this.scMain.Panel2.Controls.Add(this.scSecond);
			this.scMain.Panel2.Controls.Add(this.toolStrip1);
			this.scMain.Size = new System.Drawing.Size(850, 541);
			this.scMain.SplitterDistance = 207;
			this.scMain.TabIndex = 0;
			// 
			// tctCollections
			// 
			this.tctCollections.Alignment = System.Windows.Forms.TabAlignment.Left;
			this.tctCollections.Controls.Add(this.tpLayout);
			this.tctCollections.Controls.Add(this.tabPage2);
			this.tctCollections.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tctCollections.Location = new System.Drawing.Point(0, 33);
			this.tctCollections.Multiline = true;
			this.tctCollections.Name = "tctCollections";
			this.tctCollections.SelectedIndex = 0;
			this.tctCollections.Size = new System.Drawing.Size(207, 508);
			this.tctCollections.TabIndex = 1;
			// 
			// tpLayout
			// 
			this.tpLayout.Location = new System.Drawing.Point(23, 4);
			this.tpLayout.Name = "tpLayout";
			this.tpLayout.Padding = new System.Windows.Forms.Padding(3);
			this.tpLayout.Size = new System.Drawing.Size(180, 500);
			this.tpLayout.TabIndex = 0;
			this.tpLayout.Text = "Layout";
			this.tpLayout.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Location = new System.Drawing.Point(23, 4);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(180, 500);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// pnlCollectionHeader
			// 
			this.pnlCollectionHeader.Controls.Add(this.label1);
			this.pnlCollectionHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlCollectionHeader.Location = new System.Drawing.Point(0, 0);
			this.pnlCollectionHeader.Name = "pnlCollectionHeader";
			this.pnlCollectionHeader.Size = new System.Drawing.Size(207, 33);
			this.pnlCollectionHeader.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(207, 33);
			this.label1.TabIndex = 0;
			this.label1.Text = "Collections";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// scSecond
			// 
			this.scSecond.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scSecond.Location = new System.Drawing.Point(0, 25);
			this.scSecond.Name = "scSecond";
			// 
			// scSecond.Panel1
			// 
			this.scSecond.Panel1.Controls.Add(this.rtbMainContent);
			this.scSecond.Panel1.Controls.Add(this.panel2);
			this.scSecond.Panel1.Controls.Add(this.panel1);
			this.scSecond.Size = new System.Drawing.Size(639, 516);
			this.scSecond.SplitterDistance = 426;
			this.scSecond.TabIndex = 1;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(639, 25);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(426, 35);
			this.panel1.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 482);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(426, 34);
			this.panel2.TabIndex = 1;
			// 
			// rtbMainContent
			// 
			this.rtbMainContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.rtbMainContent.Location = new System.Drawing.Point(46, 41);
			this.rtbMainContent.Name = "rtbMainContent";
			this.rtbMainContent.Size = new System.Drawing.Size(342, 435);
			this.rtbMainContent.TabIndex = 2;
			this.rtbMainContent.Text = "";
			// 
			// writerMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.scMain);
			this.Name = "writerMain";
			this.Size = new System.Drawing.Size(850, 541);
			this.scMain.Panel1.ResumeLayout(false);
			this.scMain.Panel2.ResumeLayout(false);
			this.scMain.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
			this.scMain.ResumeLayout(false);
			this.tctCollections.ResumeLayout(false);
			this.pnlCollectionHeader.ResumeLayout(false);
			this.scSecond.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.scSecond)).EndInit();
			this.scSecond.ResumeLayout(false);
			this.ResumeLayout(false);

        }

		#endregion

		private System.Windows.Forms.SplitContainer scMain;
		private System.Windows.Forms.TabControl tctCollections;
		private System.Windows.Forms.TabPage tpLayout;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Panel pnlCollectionHeader;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SplitContainer scSecond;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.RichTextBox rtbMainContent;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
	}
}
