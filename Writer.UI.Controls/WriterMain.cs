﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Writer.UI.Controls
{
  public partial class writerMain: UserControl
  {
    public string MainDocumentContent { get; set; }
    public writerMain()
    {
        InitializeComponent();
    }
  }
}
