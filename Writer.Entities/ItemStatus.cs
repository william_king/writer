﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class ItemStatus : BaseDataObject
	{
		public int ProjectId { get; set; }
		public int StatusColor { get; set; }

	}
}
