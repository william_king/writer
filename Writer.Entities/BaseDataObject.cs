﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	[Serializable]
	public class BaseDataObject
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Synopsis { get; set; }
		public string DateCreated { get; set; }

	}
}
