﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class Target : BaseDataObject
	{
		public int ProjectId { get; set; }
		public string OutlineStart { get; set; }
		public string OutlineEnd { get; set; }
		public string DraftStart { get; set; }
		public string DraftEnd { get; set; }
		public string FirstEditStart { get; set; }
		public string FirstEditEnd { get; set; }
		public string SecondEditStart { get; set; }
		public string SecondEditEnd { get; set; }
		public string FinalEditStart { get; set; }
		public string FinalEditEnd { get; set; }
	}
}
