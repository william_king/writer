﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class Revision : BaseDataObject
	{
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }
		public string RevisionDate { get; set; }
		
	}
}
