﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class LinkedMetaDataItem : BaseDataObject
	{
		public int MetaDataItemId { get; set; }
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }
		public int ForeColor { get; set; }
		public int BackColor { get; set; }
	}
}
