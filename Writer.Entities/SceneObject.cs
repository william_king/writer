﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class SceneObject : BaseDataObject
	{
		public int SceneId { get; set; }
		public int StoryObjectId { get; set; }
	}
}
