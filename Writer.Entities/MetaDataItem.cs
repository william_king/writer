﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class MetaDataItem : BaseDataObject
	{
		public byte MetaDataType { get; set; }
		public string CustomFormat { get; set; }
		public int ForeColor { get; set; }
		public int BackColor { get; set; }

		
	}
}
