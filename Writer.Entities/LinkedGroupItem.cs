﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class LinkedGroupItem : BaseDataObject
	{
		public int LinkedGroupSectionId { get; set; }
		public int GroupItemId { get; set; }
		public int ObjectType { get; set; }
		public int ObjectId { get; set; }

	}
}
