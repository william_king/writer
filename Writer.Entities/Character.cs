﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class Character : BaseDataObject
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Image { get; set; }

	}
}
