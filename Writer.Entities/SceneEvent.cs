﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class SceneEvent : BaseDataObject
	{
		public int SceneId { get; set; }
		public int StoryEventId { get; set; }
	}
}
