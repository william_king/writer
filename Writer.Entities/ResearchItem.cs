﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class ResearchItem : BaseDataObject
	{
		public int ProjectId { get; set; }
		public string Url { get; set; }
		
	}
}
