﻿using System;

namespace Writer.Entities
{
	public class ProjectLayout
	{
		public int Id { get; set; }
		public int Version { get; set; }
		public string Title { get; set; }
		public string Author { get; set; }
		public string Premise { get; set; }
		public string DateCreated { get; set; }
		public string PlannedCompleteDate { get; set; }
		public string CompleteDate { get; set; }
		public string Synopsis { get; set; }
		public string LastFullBackup { get; set; }
		public int WordTarget { get; set; }
		public string IsCompleted { get; set; }
		public string WordTargetStartDate { get; set; }
		public string WordTargetEndDate { get; set; }
		public int WordCountStart { get; set; }
		public int LastViewedSceneId { get; set; }
		public string DesktopPath { get; set; }

	}
}