﻿using System;

namespace Writer.Entities
{
	public class WritingSession
	{
		public int ProjectId { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public int WordCountTarget { get; set; }
		public int WordCount { get; set; }

	}
}