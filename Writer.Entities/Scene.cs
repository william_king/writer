﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Writer.Entities
{
	public class Scene : BaseDataObject
	{
		public int ChapterId { get; set; }
		public int PlotId { get; set; }
		public int LocationId { get; set; }
		public int ItemSectionId { get; set; }
		public int ItemStatusId { get; set; }
		public int ItemLabelId { get; set; }
		public int PointOfView { get; set; }
		public string SceneContent { get; set; }
		public int PlannedWordCount { get; set; }
		public string StartDate { get; set; }
		public string StartTime { get; set; }
		public string EndDate { get; set; }
		public string EndTime { get; set; }
		public int SortOrder { get; set; }

	}
}
