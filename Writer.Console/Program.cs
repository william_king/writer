﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Writer.Business;

namespace Writer.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			var projectLayout = new ProjectLayoutDto
			{
				Title = "MyFirstSQLLite"
			};

			//projectLayout.CreateProject();
			//projectLayout.GetProject();
			ImportProject();
			
		}

		private static void ImportProject()
		{
			ProjectLayoutDto p = null;

			List<MetaDataItemDto> metaData = new List<MetaDataItemDto>();
			List<LinkedMetaDataItemDto> linkedMetaData = new List<LinkedMetaDataItemDto>();

			StreamReader sr = new StreamReader("dt_outline.txt");
			string line;
			while ((line = sr.ReadLine()) != null)
			{
				var row = line.Split('|');
				if (row.Length > 0)
				{
					switch (row[0])
					{
						case "ProjectLayout":
						{
							var sub = row[1].Split(',');
							p = new ProjectLayoutDto()
							{
								Version = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
								Title = !string.IsNullOrEmpty(sub[1]) ? sub[1] : string.Empty,
								Author = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
								Premise = !string.IsNullOrEmpty(sub[3]) ? sub[3] : string.Empty,
								DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
								PlannedCompleteDate = !string.IsNullOrEmpty(sub[5]) ? Convert.ToDateTime(sub[5]) : DateTime.Now,
								CompleteDate = !string.IsNullOrEmpty(sub[6]) ? Convert.ToDateTime(sub[6]) : DateTime.Now,
								Synopsis = !string.IsNullOrEmpty(sub[7]) ? ConvertToRtf(sub[7]) : string.Empty,
								LastFullBackup = !string.IsNullOrEmpty(sub[8]) ? Convert.ToDateTime(sub[8]) : DateTime.Now,
								WordTarget = !string.IsNullOrEmpty(sub[9]) ? Convert.ToInt32(sub[9]) : 0,
								IsCompleted = !string.IsNullOrEmpty(sub[10]) ? Convert.ToBoolean(sub[10]) : false,
								WordTargetStartDate = !string.IsNullOrEmpty(sub[11]) ? Convert.ToDateTime(sub[11]) : DateTime.Now,
								WordTargetEndDate = !string.IsNullOrEmpty(sub[12]) ? Convert.ToDateTime(sub[12]) : DateTime.Now,
								WordCountStart = !string.IsNullOrEmpty(sub[13]) ? Convert.ToInt32(sub[13]) : 0,
								LastViewedSceneId = !string.IsNullOrEmpty(sub[14]) ? Convert.ToInt32(sub[14]) : 0
							};

						}
							break;
						case "ItemStatus":
							{
								var sub = row[1].Split(',');
								var i = new ItemStatusDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ProjectId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
									StatusColor = Color.Black 
								};

								p.ItemStatuses.Add(i);

							}
							break;
						case "ItemLabel":
							{
								var sub = row[1].Split(',');
								var i = new ItemLabelDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									Title = !string.IsNullOrEmpty(sub[1]) ? sub[1] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[2]) ? ConvertToRtf(sub[2]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[3]) ? Convert.ToDateTime(sub[3]) : DateTime.Now,
									LabelColor = Color.Black 
								};
								p.ItemLabels.Add(i);
							}
							break;
						case "ItemSection":
							{
								var sub = row[1].Split(',');
								var i = new ItemSectionDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
									SectionColor = Color.Black 

								};
								p.ItemSections.Add(i);
							}
							break;
						case "MetaDataItem":
							{
								var sub = row[1].Split(',');
								var m = new MetaDataItemDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									Title = !string.IsNullOrEmpty(sub[1]) ? sub[1] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[2]) ? ConvertToRtf(sub[2]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[3]) ? Convert.ToDateTime(sub[3]) : DateTime.Now,
									MetaDataType = (MetaDataItemTypeDto)Enum.Parse(typeof(MetaDataItemTypeDto), sub[4]),
									CustomFormat = !string.IsNullOrEmpty(sub[5]) ? sub[5] : string.Empty,
									ForeColor = Color.Black, 
									BackColor = Color.Black 
								};

								p.MetaDataItems.Add(m);



								//var lm = new LinkedMetaDataItemDto()
								//{
								//};
								//linkedMetaData.Add(lm);
							}
							break;
						case "Locations":
							{
								var sub = row[1].Split(',');
								var l = new LocationDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ProjectId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
									Image = !string.IsNullOrEmpty(sub[5]) ? sub[5] : string.Empty
								};
								p.Locations.Add(l);
							}
							break;
						case "Characters":
							{
								var sub = row[1].Split(',');
								var c = new CharacterDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									Title = !string.IsNullOrEmpty(sub[1]) ? sub[1] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[2]) ? ConvertToRtf(sub[2]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[3]) ? Convert.ToDateTime(sub[3]) : DateTime.Now,
									Name = !string.IsNullOrEmpty(sub[4]) ? sub[4] : string.Empty,
									Surname = !string.IsNullOrEmpty(sub[5]) ? sub[5] : string.Empty,
									Image = !string.IsNullOrEmpty(sub[6]) ? sub[6] : string.Empty
								};
								p.Characters.Add(c);
							}
							break;
						case "Objects":
							{
								var sub = row[1].Split(',');
								var so = new StoryObjectDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ProjectId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
									Image = !string.IsNullOrEmpty(sub[5]) ? sub[5] : string.Empty
								};
								p.StoryObjects.Add(so);
							}
							break;
						case "Events":
							{
								var sub = row[1].Split(',');
								var se = new StoryEventDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ProjectId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now
								};
								p.StoryEvents.Add(se);
							}
							break;
						case "Chapters":
							{
								var sub = row[1].Split(',');
								var c = new ChapterDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ProjectId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									Title = !string.IsNullOrEmpty(sub[2]) ? sub[2] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[3]) ? ConvertToRtf(sub[3]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[4]) ? Convert.ToDateTime(sub[4]) : DateTime.Now,
									SortOrder = !string.IsNullOrEmpty(sub[5]) ? Convert.ToInt32(sub[5]) : 0
								};
								p.Chapters.Add(c);
							}
							break;
						case "Scenes":
							{
								var sub = row[1].Split(',');
								var s = new SceneDto()
								{
									Id = !string.IsNullOrEmpty(sub[0]) ? Convert.ToInt32(sub[0]) : 0,
									ChapterId = !string.IsNullOrEmpty(sub[1]) ? Convert.ToInt32(sub[1]) : 0,
									PointOfView = p.Characters.First(c => c.Id == Convert.ToInt32(sub[7])),
									Title = !string.IsNullOrEmpty(sub[8]) ? sub[8] : string.Empty,
									Synopsis = !string.IsNullOrEmpty(sub[9]) ? ConvertToRtf(sub[9]) : string.Empty,
									DateCreated = !string.IsNullOrEmpty(sub[10]) ? Convert.ToDateTime(sub[10]) : DateTime.Now,
									SceneContent = !string.IsNullOrEmpty(sub[11]) ? sub[11] : string.Empty,
									PlannedWordCount = !string.IsNullOrEmpty(sub[12]) ? Convert.ToInt32(sub[12]) : 0,
									StartDate = !string.IsNullOrEmpty(sub[13]) ? Convert.ToDateTime(sub[13]) : DateTime.Now,
									StartTime = !string.IsNullOrEmpty(sub[14]) ? Convert.ToDateTime(sub[14]) : DateTime.Now,
									EndDate = !string.IsNullOrEmpty(sub[15]) ? Convert.ToDateTime(sub[15]) : DateTime.Now,
									EndTime = !string.IsNullOrEmpty(sub[16]) ? Convert.ToDateTime(sub[16]) : DateTime.Now,
									SortOrder = !string.IsNullOrEmpty(sub[4]) ? Convert.ToInt32(sub[17]) : 0
								};

								RetrieveSceneContent(s);

								s.ScenePlot = null;
								s.SceneLocation = p.Locations.First(l => l.Id == Convert.ToInt32(sub[3]));
								s.Section = p.ItemSections.First(x => x.Id == Convert.ToInt32(sub[4]));
								s.Status = p.ItemStatuses.First(x => x.Id == Convert.ToInt32(sub[5]));
								s.Label = p.ItemLabels.First(x => x.Id == Convert.ToInt32(sub[6]));

								p.Chapters.First(x => x.Id == s.ChapterId).Scenes.Add(s);
							}
							break;
					};
				}
				
			}

			sr.Close();

			p.DesktopPath = @"H:\Projects\Writer\Writer.UI\bin\Debug\DT";

			p.CreateProject();
		}

		private static void RetrieveSceneContent(SceneDto s)
		{
			RichTextBox rtb = new RichTextBox()
			{
				Text = File.ReadAllText(@"H:\Projects\Writer\Writer.UI\bin\Debug\DT\" + s.Title + ".txt")
			};
			s.SceneContent = rtb.Rtf;

		}

		private static string ConvertToRtf(string text)
		{
			RichTextBox rtb = new RichTextBox()
			{
				Text = text
			};
			return rtb.Rtf;
		}
	}
}
